package com.uqu.login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.uqu.mapmytrip.MainActivity;

import com.uqu.mapmytrip.MapMyTripApp;
import com.simplicity.io.mapmytrip.R;
import com.uqu.mapmytrip.MapTripUtility;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.message.BasicHeader;
import org.json.JSONException;
import org.json.JSONObject;

import org.apache.http.entity.StringEntity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * 
 * Login Activity Class 
 *
 */
public class LoginActivity extends Activity {
	// Progress Dialog Object
	ProgressDialog prgDialog;
	// Error Msg TextView Object
	TextView errorMsg;
	// Email Edit View Object
	EditText userNameET;
	// Passwprd Edit View Object
	EditText pwdET;

	Driver driver;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		// Find Error Msg Text View control by ID
		errorMsg = (TextView)findViewById(R.id.login_error);
		// Find Email Edit View control by ID
		userNameET = (EditText)findViewById(R.id.loginUserName);
		// Find Password Edit View control by ID
		pwdET = (EditText)findViewById(R.id.loginPassword);
		// Instantiate Progress Dialog object
		prgDialog = new ProgressDialog(this);
		// Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);
	}
	
	/**
	 * Method gets triggered when Login button is clicked
	 * 
	 * @param view
	 */
	public void loginUser(View view){
		// Get Email Edit View Value
		String userName = userNameET.getText().toString();
		// Get Password Edit View Value
		String password = pwdET.getText().toString();
		// Instantiate Http Request Param Object
		RequestParams params = new RequestParams();
		// When UserName Edit View and Password Edit View have values other than Null
		if(Utility.isNotNull(userName) && Utility.isNotNull(password)){
			driver=new Driver();
			driver.setDriverId(userName);
			driver.setPassword(password);
			new HttpAsyncTask().execute(MapTripUtility.baseURL+"dologin.htm");

		}else{
			Toast.makeText(getApplicationContext(), "Please fill the form, don't leave any field blank", Toast.LENGTH_LONG).show();
		}
		
	}

	public void navigatetoDriverDetailActivity(View view){
		Intent loginIntent = new Intent(getApplicationContext(),MainActivity.class);
		loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(loginIntent);
	}
	public String POST(String url, Driver driver){

		InputStream inputStream = null;
		String result = "";
		try {

			// 1. create HttpClient
			HttpClient httpclient = new DefaultHttpClient();

			// 2. make POST request to the given URL
			HttpPost httpPost = new HttpPost(url);

			String json = "";

			// 3. build jsonObject
			Gson gson =new Gson();
			json=gson.toJson(driver);
			// 5. set json to StringEntity
			StringEntity se = new StringEntity(json);

			// 6. set httpPost Entity
			httpPost.setEntity(se);

			// 7. Set some headers to inform server about the type of the content
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");

			// 8. Execute POST request to the given URL
			HttpResponse httpResponse = httpclient.execute(httpPost);

			int statusCode = httpResponse.getStatusLine().getStatusCode();

			Log.d("status code",""+statusCode);

			if (statusCode!=200) {
				if (statusCode == 404) {
					return "" + statusCode;
				}
				// When Http response code is '500'
				else if (statusCode == 500) {
					return "" + statusCode;
				}
				// When Http response code other than 404, 500
				else {

					return ""+2000;
				}
			}

			// 9. receive response as inputStream
			inputStream = httpResponse.getEntity().getContent();

			// 10. convert inputstream to string
			if(inputStream != null)
				result = convertInputStreamToString(inputStream);
			else
				result = "Did not work!";

		} catch (Exception e) {
			Log.d("InputStream", e.getLocalizedMessage());
			result=e.getLocalizedMessage();
		}

		// 11. return result
		return result;
	}
	private static String convertInputStreamToString(InputStream inputStream) throws IOException {
		BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;

	}
	private class HttpAsyncTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			prgDialog.show();
		}

		@Override
		protected String doInBackground(String... urls) {

			return POST(urls[0], driver);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			prgDialog.hide();
			if (result!=null && result.contains(driver.getDriverId())){
				Gson gson=new Gson();
				Driver tmpDriver=gson.fromJson(result,Driver.class);
				if (tmpDriver.getId()>0) {
					finish();
					gotoMapMyTripApp(tmpDriver.getId());
				}else{
					errorMsg.setText("Invalid User Name/password");
				}
			}else{
				if (result.contains("404"))
				Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
				else
					if (result.contains("505"))
						Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
					else
						if(result.contains("2000") || result.contains("refused"))
							Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
						else
							errorMsg.setText("Invalid User Name/password");
			}
		}

	}
	public void gotoMapMyTripApp(View view) {
		finish();
		Intent intent = new Intent(LoginActivity.this, MapMyTripApp.class);
		LoginActivity.this.startActivity(intent);
	}
	void gotoMapMyTripApp(long id) {
		finish();
		Intent intent = new Intent(LoginActivity.this, MapMyTripApp.class);
		intent.putExtra("driverId",id);
		LoginActivity.this.startActivity(intent);
	}

}
