package com.uqu.login;

public class Driver {
	
	
	public long getId(){
		return id;
	}
	public void setId(long id){this.id=id;}
	public String getDriverId() {return driverId;}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getNationality() {return nationality;	}
	public void setNationality(String nationality) {this.nationality = nationality;}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getEducationLevel() {
		return educationLevel;
	}
	public void setEducationLevel(String educationLevel) {
		this.educationLevel = educationLevel;
	}
	public String getDriverCategory() {
		return driverCategory;
	}
	public void setDriverCategory(String driverCategory) {
		this.driverCategory = driverCategory;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDriverExperience() {
		return driverExperience;
	}
	public void setDriverExperience(String driverExperience) {this.driverExperience = driverExperience;}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getEyeGlasses() {
		return eyeGlasses;
	}
	public void setEyeGlasses(String eyeGlasses) {
		this.eyeGlasses = eyeGlasses;
	}
	public String getAgeRange() {
		return ageRange;
	}
	public void setAgeRange(String ageRange) {
		this.ageRange = ageRange;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	private long id;
	private String driverId;
	private String driverName;
	private String nationality;
	private String password;
	private String deviceId;
	private String educationLevel;
	private String driverCategory;
	private String gender;
	private String driverExperience;
	private String vehicleType;
	private String eyeGlasses;
	private String ageRange;
	private String remarks;

}
