package com.uqu.camerarecorder;

import android.animation.ValueAnimator;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import java.util.Date;
;

public class BackgroundVideoRecorder extends Service {

    private WindowManager windowManager;
    private Camera mCamera;
    private MediaRecorder mediaRecorder = null;
    private TextureView mTexture;
    private WindowManager.LayoutParams params;
    private final IBinder mBinder = new LocalBinder();

    private int tappedItemId = -1;
    Handler myTapHandler;
    final Context ctx = this;

    String tripName;
    boolean isMaximized=false;

    private TextureView.SurfaceTextureListener mSurface = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            try {
                mediaRecorder = new MediaRecorder();
                if (checkCameraHardware(getApplicationContext()))
                    mCamera = getCameraInstance();

                setCameraDisplayOrientation(getApplicationContext(), 1, mCamera);
                mCamera.unlock();

                mediaRecorder.setPreviewDisplay(new Surface(surface));
                mediaRecorder.setCamera(mCamera);
                //mCamera.setPreviewTexture(surface);
                //mCamera.startPreview();

                mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
                mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
                mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_LOW));
                String fileName="unknown_trip_recording_"+new Date()+".mp4";

                if (tripName!=null){
                    fileName=tripName+"_";
                    fileName+= new Date()+".mp4";
                }
                mediaRecorder.setOutputFile(Environment.getExternalStorageDirectory() + "/MapMyTrip/"+fileName);

                //mediaRecorder.setMaxDuration(1 * 60 * 1000);


                try {
                    mediaRecorder.prepare();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mediaRecorder.start();
            }
            catch (Exception t) {
                t.printStackTrace();
            }
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return true;
        }


        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
            try {

            } catch (Exception e){
                // ignore: tried to stop a non-existent preview
            }

            try {
                //mCamera.setPreviewTexture(surface);
                mediaRecorder.setPreviewDisplay(new Surface(surface));
            } catch (Exception t) {
                t.printStackTrace();
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        params = new WindowManager.LayoutParams();
        params.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        params.format = PixelFormat.TRANSLUCENT;
        params.gravity = Gravity.LEFT | Gravity.TOP;
        params.x =0; //getResources().getDisplayMetrics().widthPixels;;
        params.height = 150;
        params.width = 150;
        params.y =0; //getResources().getDisplayMetrics().heightPixels;

        params.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH |
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED;


        mTexture = new TextureView(this);
        mTexture.setSurfaceTextureListener(mSurface);
    /*mParentView = new FrameLayout(getApplicationContext());

    mParentView.addView(mTexture);*/
        windowManager.addView(mTexture, params);

        fadeIn(mTexture, params);
        overlayAnimation(mTexture, 0, getResources().getDisplayMetrics().heightPixels);


        int i=10;
        int n=5;
        mTexture.setId(i*1000+n);


        mTexture.setOnTouchListener(new View.OnTouchListener() {
            int lastX, lastY;
            int paramX, paramY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        lastX = (int) event.getRawX();
                        lastY = (int) event.getRawY();
                        paramX = params.x;
                        paramY = params.y;

                        break;
                    }

                    case MotionEvent.ACTION_UP: {

                        lastX = (int) event.getRawX();
                        lastY = (int) event.getRawY();
                        paramX = params.x;
                        paramY = params.y;


                        //active 'tap handler' for current id?
                        if(myTapHandler != null && myTapHandler.hasMessages(v.getId())) {

                            // clean up (to avoid single tap msg to be send and handled)
                            myTapHandler.removeMessages(tappedItemId);
                            tappedItemId = -1;

                            if (isMaximized){
                                isMaximized=false;
                                params.height=150;
                                params.width=150;

                            }else{
                                isMaximized=true;
                                params.width= getResources().getDisplayMetrics().widthPixels;
                                params.height=getResources().getDisplayMetrics().heightPixels;
                            }
                            //mTexture.setLayoutParams(params);
                            windowManager.updateViewLayout(mTexture, params);

                            return true;
                        } else {
                            tappedItemId = v.getId();
                            myTapHandler = new Handler(){
                                public void handleMessage(Message msg){
                                    //Toast.makeText(ctx, "single tap on "+ tappedItemId + " msg 'what': " + msg.what, Toast.LENGTH_SHORT).show();
                                }
                            };

                            Message msg = Message.obtain();
                            msg.what = tappedItemId;
                            msg.obj = new Runnable() {
                                public void run() {
                                    //clean up
                                    tappedItemId = -1;
                                }
                            };
                            myTapHandler.sendMessageDelayed(msg, 350); //350ms delay (= time to tap twice on the same element)
                        }
                        break;
                    }
                    case MotionEvent.ACTION_MOVE: {
                        int dx = (int) event.getRawX() - lastX;
                        int dy = (int) event.getRawY() - lastY;
                        params.x = paramX + dx;
                        params.y = paramY + dy;



                        windowManager.updateViewLayout(mTexture, params);
                        break;
                    }

                }


                return true;
            }
        });

    }


    public class LocalBinder extends Binder {
        public BackgroundVideoRecorder getService() {
            // Return this instance of LocalService so clients can call public methods
            return BackgroundVideoRecorder.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Not used
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        windowManager.removeView(mTexture);
        super.onDestroy();
        releaseMediaRecorder();
        mCamera.lock();
        mCamera.release();



    }

    /** Check if this device has a camera */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            Log.e("camera", "Failed to access camera");
            e.printStackTrace();
        }
        return c; // returns null if camera is unavailable
    }

    public void setCameraDisplayOrientation(Context activity, int cameraId, Camera camera) {
        Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        windowManager = (WindowManager) activity.getSystemService(WINDOW_SERVICE);
        int rotation = windowManager.getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }
    private  float distance(float x1, float y1, float x2, float y2) {
        float dx = x1 - x2;
        float dy = y1 - y2;
        float distanceInPx = (float) Math.sqrt(dx * dx + dy * dy);
        return pxToDp(distanceInPx);
    }

    private  float pxToDp(float px) {
        return px /  getResources().getDisplayMetrics().density;
    }

    public void setTripName(String currentTripNamae){

        tripName=currentTripNamae;
    }

    public void showWindow(){

        params.height=150;
        params.width=150;
        windowManager.updateViewLayout(mTexture, params);
    }

    public void hideWindow(){

        params.height=1;
        params.width=1;
        windowManager.updateViewLayout(mTexture,params);
    }
    public void releaseMediaRecorder(){
        try {
            mediaRecorder.stop();
            mediaRecorder.reset();
            mediaRecorder.release();

        }catch (Exception e){
            e.printStackTrace();
        }
    }
final static int ANIMATION_DURATION=1000;
    public  void overlayAnimation(final View view2animate, int viewX, int endX) {
        ValueAnimator translateLeft = ValueAnimator.ofInt(viewX, endX);
        translateLeft.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                updateViewLayout(view2animate, null, val, null, null);

            }
        });
        translateLeft.setDuration(ANIMATION_DURATION);
        translateLeft.start();
    }

    public  void updateViewLayout(View view, Integer x, Integer y, Integer w, Integer h){
        if (view!=null) {
            WindowManager.LayoutParams lp = (WindowManager.LayoutParams) view.getLayoutParams();

            if(x != null)lp.x=x;
            if(y != null)lp.y=y;
            if(w != null && w>0)lp.width=w;
            if(h != null && h>0)lp.height=h;

            windowManager.updateViewLayout(view, lp);
        }
    }



    /*This will handle the first time call*/
    public void fadeIn(final View notificationView, final WindowManager.LayoutParams param){
        final long startTime = System.currentTimeMillis();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                fadeInHandler(notificationView, param, startTime);
            }
        }, 25);
    }

    /*This will handle the entire animation*/
    public void fadeInHandler(final View notificationView, final WindowManager.LayoutParams param, final long startTime){
        try {
            long timeNow = System.currentTimeMillis();
            float alpha = ((timeNow - startTime) / 10000.0f) * 1.0f;
            if (alpha >= 1.0f) {
                alpha = 1.0f;
            }
            params.alpha = alpha;
            windowManager.updateViewLayout(notificationView, param);
            if (timeNow - startTime < 10000) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        fadeInHandler(notificationView, param, startTime);
                    }
                }, 25);
            }
        }catch (Exception e){}
    }


}