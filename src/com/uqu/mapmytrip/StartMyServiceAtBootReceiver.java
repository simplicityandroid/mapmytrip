package com.uqu.mapmytrip;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StartMyServiceAtBootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
				Intent startActivity = new Intent();
				startActivity.setClass(context, MapMyTripApp.class);
				startActivity.setAction(MapMyTripApp.class.getName());
				startActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
				context.startActivity(startActivity);
			} 
		} catch (Exception e) {
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_ERROR, e.getLocalizedMessage());
		}
	}
}