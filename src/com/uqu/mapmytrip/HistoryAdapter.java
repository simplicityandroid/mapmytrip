package com.uqu.mapmytrip;

import java.text.SimpleDateFormat;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.simplicity.io.mapmytrip.R;

public class HistoryAdapter extends CursorAdapter {

	public static final String TAG = "HistoryAdapter";
	Context context;
	Cursor cursor;
	private ViewHolder holder;
	public static String imageHeaderUrl;
	
	@SuppressWarnings("deprecation")
	public HistoryAdapter(Context context, Cursor c) {
		super(context, c, false);
		this.context = context;
        this.cursor = c;
        
        //this.HistoryAdapter = (MapMyHajjApp) context;
	}
	
	private static class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
    }

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		//Log.i(TAG, "Going to call newView");
		
        View v = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.maptrip_history_layout, parent, false);
		v.setTag(new ViewHolder());
		bindView(v, context, cursor);
		return v;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		holder = (ViewHolder)view.getTag();
        
		holder.txtTitle = (TextView) view.findViewById(R.id.tripname);
        holder.imageView = (ImageView) view.findViewById(R.id.maptriplogo);
		
		String tripName = getCursor().getString(getCursor().getColumnIndex(
					MapTripDatabase.RecordColumn.TRIPNAME));
		
		String tripDate = getCursor().getString(getCursor().getColumnIndex(
					MapTripDatabase.RecordColumn.TIMESTAMP));
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSS");
		String formattedDate = formatter.format(Long.parseLong(tripDate));
		holder.txtTitle.setText(tripName + "_" + formattedDate);
	}

	@Override
	public Object getItem(int position) {
		Cursor c = getCursor();
		c.moveToPosition(position);
		return (c);
	}
}
