package com.uqu.mapmytrip;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

import com.simplicity.io.mapmytrip.R;

/**
 * A login screen that offers login via email/password.
 */
public class DriverDetailActivity extends Activity {

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	// UI references.

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_driver_detail);
        
		Button mStartButton = (Button) findViewById(R.id.btnStart);
		mStartButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				startMainActivity();
			}
		});

		Button mSkipButton = (Button) findViewById(R.id.btnSkip);
		mSkipButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				startTrip();
			}
		});

	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void startMainActivity() {

		startActivity(new Intent(DriverDetailActivity.this, MainActivity.class));
	}
	public void startTrip() {
		// make sure we close the start screen so the user won't come back when it presses back key

		finish();
		// start the home screen

		Intent intent = new Intent(DriverDetailActivity.this, MapMyTripApp.class);
		DriverDetailActivity.this.startActivity(intent);
	}

}
