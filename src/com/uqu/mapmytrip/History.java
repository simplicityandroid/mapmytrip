package com.uqu.mapmytrip;

public class History {
	String title;
	int tripId;

	public History() {
		super();
	}
	
	public History(String title, int id) {
		super();
		this.title = title;
		this.tripId = id;
	}
	
	public int getId() {
		return tripId;
	}
	public void setId(int id) {
		this.tripId = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

}
