package com.uqu.mapmytrip;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MyWebViewClient extends WebViewClient {

  public MyWebViewClient(WebActivity webActivity) {
     super();
     //start anything you need to
  }

  @Override
  public void onPageStarted(WebView view, String url, Bitmap favicon) {
     //Do something to the urls, views, etc.
	  super.onPageStarted(view, url, favicon);
  }
  
  @Override
  public boolean shouldOverrideUrlLoading(WebView view, String url) {
      // TODO Auto-generated method stub

      view.loadUrl(url);
      return true;

  }
 }