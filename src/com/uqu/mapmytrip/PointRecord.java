package com.uqu.mapmytrip;

public class PointRecord {
	private int id;
	String timeStamp;
	String postTimeStamp;
	public String getPostTimeStamp() {
		return postTimeStamp;
	}

	public void setPostTimeStamp(String postTimeStamp) {
		this.postTimeStamp = postTimeStamp;
	}

	String networkType;
	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	private Double latitude;
	private Double longitude;
	
	private Double altitude;
	private Double accuracy;
	private Double speed;
	private Double bearing;
	private String uid;
	protected float accX;
	protected float accY;
	protected float accZ;
	protected float gyrX;
	protected float gyrZ;
	protected float gyrY;
	protected float light;
	protected float magX;
	protected float magY;
	protected float magZ;
	private String operator;
	private int signalStrength;
	private long driverId;

	public long getDriverId(){return driverId;}
	public void setDriverId(long driverId){this.driverId=driverId;}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public int getSignalStrength() {
		return signalStrength;
	}

	public float getAccX() {
		return accX;
	}

	public void setAccX(float accX) {
		this.accX = accX;
	}

	public float getAccY() {
		return accY;
	}

	public void setAccY(float accY) {
		this.accY = accY;
	}

	public float getAccZ() {
		return accZ;
	}

	public void setAccZ(float accZ) {
		this.accZ = accZ;
	}

	public float getGyrX() {
		return gyrX;
	}

	public void setGyrX(float gyrX) {
		this.gyrX = gyrX;
	}

	public float getGyrZ() {
		return gyrZ;
	}

	public void setGyrZ(float gyrZ) {
		this.gyrZ = gyrZ;
	}

	public float getGyrY() {
		return gyrY;
	}

	public void setGyrY(float gyrY) {
		this.gyrY = gyrY;
	}

	public float getLight() {
		return light;
	}

	public void setLight(float light) {
		this.light = light;
	}

	public float getMagX() {
		return magX;
	}

	public void setMagX(float magX) {
		this.magX = magX;
	}

	public float getMagY() {
		return magY;
	}

	public void setMagY(float magY) {
		this.magY = magY;
	}

	public float getMagZ() {
		return magZ;
	}

	public void setMagZ(float magZ) {
		this.magZ = magZ;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getTimeStamp() {
		return this.timeStamp;
	}
	
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public Double getLatitude() {
		return this.latitude;
	}
	
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public Double getLongitude() {
		return this.longitude;
	}
	
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public double getAltitude() {
		return this.altitude;
	}
	
	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}
	
	public double getAccuracy() {
		return this.accuracy;
	}
	
	public void setAccuracy(Double accuracy) {
		this.accuracy = accuracy;
	}
	
	public double getSpeed() {
		return this.speed;
	}
	
	public void setSpeed(Double speed) {
		this.speed = speed;
	}
	
	public double getBearing() {
		return this.bearing;
	}
	
	public void setBearing(Double bearing) {
		this.bearing = bearing;
	}

	public void setOperatorName(String operatorName) {
		this.operator = operatorName;
	}

	public void setSignalStrength(int mSignal) {
		this.signalStrength = mSignal;
	}
}
