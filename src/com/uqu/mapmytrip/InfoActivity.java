package com.uqu.mapmytrip;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import com.simplicity.io.mapmytrip.R;

public class InfoActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Configuration configuration = getResources().getConfiguration();
		if ((configuration.screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
			setContentView(R.layout.info_layout);
		}
		else {
			setContentView(R.layout.info_layout);
		}
		
		this.setTitle("Map My Trips");
		
		TextView licenseText = (TextView) findViewById(R.id.textView2);
        String tempText = (String)licenseText.getText();
        licenseText.setText(Html.fromHtml(tempText));
	}
}