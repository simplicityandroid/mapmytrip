package com.uqu.mapmytrip;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.os.Build;

import com.simplicity.io.mapmytrip.R;

public class WebActivity extends Activity {

	private WebView webview;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_webview);

	    String url = "http://54.172.33.121:8080/tawaaf";

	    webview = (WebView) findViewById(R.id.myWebView);
	    //next line explained below
	    final Activity activity = this;

	    webview.getSettings().setJavaScriptEnabled(true);     
	    webview.getSettings().setLoadWithOverviewMode(true);
	    webview.getSettings().setUseWideViewPort(true);     
//	    webview.getSettings().setUseWideViewPort(true);
	    webview.getSettings().setBuiltInZoomControls(true);
	    webview.getSettings().setPluginState(PluginState.ON);
	    webview.setWebChromeClient(new WebChromeClient() {
	    	   public void onProgressChanged(WebView view, int progress) {
	    	     // Activities and WebViews measure progress with different scales.
	    	     // The progress meter will automatically disappear when we reach 100%
	    	     activity.setProgress(progress * 1000);
	    	   }
	    	 });
	    webview.setWebViewClient(new MyWebViewClient(this));
	    webview.loadUrl(url);
	}
	@Override
    public void onBackPressed() {
        if(webview.canGoBack()) {
        	webview.goBack();
        } else {
            super.onBackPressed();
        }
    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.web, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_web, container,
					false);
			return rootView;
		}
	}

}
