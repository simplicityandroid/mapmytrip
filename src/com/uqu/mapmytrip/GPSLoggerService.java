package com.uqu.mapmytrip;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.simplicity.io.mapmytrip.R;

import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

public class GPSLoggerService extends Service {

	//private final DecimalFormat sevenSigDigits = new DecimalFormat("0.#######")
	private Handler mHandler;
	private LocationManager locationManager;
	private LocationListener locationListener;
	private SensorManager mSensorManager;
	private MapTripDatabase db;

	private static long minTimeMillis = 30000;
	private static long minDistanceMeters = 5;            //1
	private static float minAccuracyMeters = 35;        //100;

	private int lastStatus = 0;
	private static boolean showingDebugToast = false;

	private static final String tag = "GPSLoggerService";
	private NotificationManager mNM;
	public Sensor mAccelerometer;
	public Sensor mGyroscope;
	public Sensor mLight;
	public Sensor mMagnetic;
	private boolean firstTime = true;
	private AlertDialog networkDialog;
	private AlertDialog gpsDialog;

	// This is the object that receives interactions from clients. See
	// RemoteService for a more complete example.
	private final IBinder mBinder = new LocalBinder();
	protected float accX = -1;
	protected float accY = -1;
	protected float accZ = -1;
	protected float gyrX = -1;
	protected float gyrZ = -1;
	protected float gyrY = -1;
	protected float light = -1;
	protected float magX = -1;
	protected float magY = -1;
	protected float magZ = -1;
	private Timer timer;
	private Timer timerLocationDataSender;
	private Timer timerLocationDataCollector;
	private static int mSignal = 0;
	private MyPhoneStateListener mSignalListener;
	private TelephonyManager telManager;
	public Location lastKnownLocation;
	private static Context mContext;
	private long driverId;
	TimerTask timerTask;
	TimerTask postDataTimerTask;
	boolean stopServices;

	public static Context getContext() {
		if (GPSLoggerService.mContext == null) {
			throw new IllegalStateException("Service is not been initialized!");
		}

		return GPSLoggerService.mContext;
	}

	// Called when activity first created.
	private void startLoggerService() {
		//use the LocationManager class to obtain GPS locations
		try {
			mContext = this;
			Log.i(tag, "MapMyTrip GPS service started...");
			MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_INFO, "Location collection service started...");
			mHandler = new Handler();
			mSignalListener = new MyPhoneStateListener();
			telManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
			telManager.listen(mSignalListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);

			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

			// Getting Current Location

			locationListener = new MyLocationListener();
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
					minTimeMillis, 0, locationListener);
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
					minTimeMillis, 0, locationListener);


			mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
			if (mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
				mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
				mSensorManager.registerListener(mSensorListener, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
				// Success! There's a accelerometer sensor.
			} else {
				// Failure! No accelerometer sensor.
			}
			if (mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null) {
				mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
				mSensorManager.registerListener(mSensorListener, mGyroscope, SensorManager.SENSOR_DELAY_NORMAL);
				// Success! There's a GYROSCOPE sensor.
			} else {
				// Failure! No GYROSCOPE sensor.
			}
			if (mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT) != null) {
				mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
				mSensorManager.registerListener(mSensorListener, mLight, SensorManager.SENSOR_DELAY_NORMAL);
				// Success! There's a LIGHT sensor.
			} else {
				// Failure! No LIGHT sensor.
			}
			if (mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null) {
				mMagnetic = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
				mSensorManager.registerListener(mSensorListener, mMagnetic, SensorManager.SENSOR_DELAY_NORMAL);
				// Success! There's a LIGHT sensor.
			} else {
				// Failure! No LIGHT sensor.
			}
			MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_INFO, "Sensors collection service started...");

			timer = new Timer("Sensors Notification");

			timer.schedule(new TimerTask() {
				public void run() {
					{
						try {
							SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(GPSLoggerService.getContext());
							String deviceUniqueId = sharedPreferences.getString(MapMyTripApp.wifiKey, "0");
							if (deviceUniqueId == "0" || !deviceUniqueId.contains(":")) {
								MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_INFO, "About to Register New Device while sensors collection...");
								new VehicleRegistrationTask(GPSLoggerService.getContext()).execute();
							}
//					Time now = new Time();
//					now.setToNow();
							TimeZone tz = TimeZone.getTimeZone("GMT+03:00");
							Calendar c = Calendar.getInstance(tz);
							String sqlStatement = "INSERT INTO `mapmytrip`.`sensors` (`device_id`, `time`, `accx`, `accy`, `accz`, `gyrx`, `gyry`,`gyrz`, `magx`, `magy`, `magz`, `light`) VALUES ('" +
									deviceUniqueId + "'," + "STR_TO_DATE('" + MapTripUtility.timestampFormat.format(c.getTimeInMillis()) + "','%Y%m%d%H%i%s%p'),'" + accX + "','" + accY + "','" + accZ + "','" + gyrX + "','" + gyrY + "','" + gyrZ + "','" + magX + "','" + magY + "','" + magZ + "','" + light + "');";

							MapMyTripApp.sqlSensorToFile(GPSLoggerService.getContext(), sqlStatement);
						} catch (Exception e) {

						}

					}
				}
			}, 0, 1 * 1000L);





			db = MapTripDatabase.getInstance();
			if (db == null) {
				MapTripDatabase.init(mContext);
				db = MapTripDatabase.getInstance();
			}
			if (db != null) {
				Log.i(tag, "Database opened ok");
				MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_INFO, "Database opened Ok...");
			}
		} catch (Exception e) {

		}

	}

	public String getOperatorName() {
		String operatorName = "UNKNOWN";
		TelephonyManager telephonyManager = ((TelephonyManager) GPSLoggerService.getContext().getSystemService(Context.TELEPHONY_SERVICE));
		if (telephonyManager != null)
			operatorName = telephonyManager.getNetworkOperatorName();
		return operatorName;
	}


	public String get_network() {
		String network_type = "UNKNOWN";// maybe usb reverse tethering
		NetworkInfo active_network = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
		if (active_network != null && active_network.isConnectedOrConnecting()) {
			if (active_network.getType() == ConnectivityManager.TYPE_WIFI) {
				network_type = "WIFI";
			} else if (active_network.getType() == ConnectivityManager.TYPE_MOBILE) {
				network_type = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo().getSubtypeName();
				if (network_type == null)
					network_type = getNetworkType();
			}
		}
		return network_type;
	}

	private String getNetworkType() {
		TelephonyManager teleMan = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		int networkType = teleMan.getNetworkType();

		switch (networkType) {
			case 7:
				return "1xRTT";
			case 4:
				return "CDMA";
			case 2:
				return "EDGE";
			case 14:
				return "eHRPD";
			case 5:
				return "EVDO rev. 0";
			case 6:
				return "EVDO rev. A";
			case 12:
				return "EVDO rev. B";
			case 1:
				return "GPRS";
			case 8:
				return "HSDPA";
			case 10:
				return "HSPA";
			case 15:
				return "HSPA+";
			case 9:
				return "HSUPA";
			case 11:
				return "iDen";
			case 13:
				return "LTE";
			case 3:
				return "UMTS";
			case 0:
				return "Unknown";
		}
		return "Unknown";
	}

	public boolean isOnline() {
		if (checkGPSStatus()) {
			return true;
		}
		return false;
	}

	// Check Wi-Fi is on
	boolean confirmWiFiAvailable(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		return wifiInfo.isAvailable() && wifiInfo.isConnected() && wifiState(context);
	}


	public static boolean wifiState(Context context) {
		WifiManager mng = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		return mng.isWifiEnabled();
	}

	boolean internetState(Context context) {
	ConnectivityManager cm =(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
	return  activeNetwork != null && activeNetwork.isConnected();

}

	// Check Wi-Fi is on
	boolean confirmGPRSAvailable(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo gprsInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (gprsInfo != null)
			return gprsInfo.isAvailable();
		return false;
	}
	// Check Airplane Mode - we want airplane mode off
	boolean confirmAirplaneModeOff(Context context) {
		int airplaneSetting = Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0);
		return airplaneSetting == 0;
	}
	
	boolean isNetLocUsable(Context context) {
		return (internetState(context) && confirmAirplaneModeOff(context));
	}
	
	private boolean checkGPSStatus() {
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		boolean status = true;
		/*
		 * try to see if the location stored on android is close enough for
		 * you,and avoid re-requesting the location
		 */
		try {
		if (!isNetLocUsable(GPSLoggerService.getContext()) ) {
//			AlertDialog.Builder builder = new AlertDialog.Builder(GPSLoggerService.getContext());
//			final String action = Settings.ACTION_WIRELESS_SETTINGS;
//			final String message = "Enable Wifi/Cellular Data to Sync location data with Server"
//					+ " Click Settings to go to Wifi settings."
//					+ " Application will not perform correctly in the absence of Network";
//
//			builder.setMessage(message)
//					.setPositiveButton("Settings",
//							new DialogInterface.OnClickListener() {
//								public void onClick(DialogInterface d, int id) {
//									GPSLoggerService.getContext().startActivity(new Intent(action));
//									d.dismiss();
//								}
//							})
//					.setNegativeButton("Cancel",
//							new DialogInterface.OnClickListener() {
//								public void onClick(DialogInterface d, int id) {
//									d.cancel();
//								}
//							});
//
//			networkDialog = builder.create();
//			networkDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
//			networkDialog.show();
			MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_ERROR, "Internet is Disabled or Device is on Airplane mode...");
			status = false;
		}
		else
			MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_INFO, "Internet is Ok...");
		}
		catch (Exception e) {
			MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_ERROR, "Error while checking Internet:"+e.getMessage());
		}

		boolean gpsLocation = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

		if (!gpsLocation && (gpsDialog == null || !gpsDialog.isShowing())) {
			try {
			AlertDialog.Builder builder = new AlertDialog.Builder(GPSLoggerService.getContext());
			final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
			final String message = "Enable GPS service to collect locations information"
					+ " Click Settings to go to GPS settings."
					+ " Application will not perform correctly in the absence of GPS";


			builder.setMessage(message)
					.setPositiveButton("Settings",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface d, int id) {
									Intent i = new Intent(action);
									i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
									GPSLoggerService.getContext().startActivity(i);
									d.dismiss();
								}
							})
					.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface d, int id) {
//									if (locationManager != null) {
//										locationManager.removeUpdates(locationListener);
//										locationManager = null;
//									}
//
//									if (locationListener != null) {
//										locationListener = null;
//									}
									d.cancel();
								}
							});

			gpsDialog = builder.create();
			gpsDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
			gpsDialog.show();
			}
			catch (Exception e) {
				MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_ERROR, "Show GPS Dialog Error:"+e.getMessage());
			}
			MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_ERROR, "GPS is Disabled...");
			//status = false;

		} else {
			MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_INFO, "GPS is Ok...");
			// do your handling if the last known location stored on android is
			// enouh for you
		}
		
		return status;
	}

	private class MyPhoneStateListener extends PhoneStateListener {
		/*
		 * Get the Signal strength from the provider, each tiome there is an
		 * update
		 */
		@Override
		public void onSignalStrengthsChanged(SignalStrength signalStrength) {
			super.onSignalStrengthsChanged(signalStrength);
			mSignal = signalStrength.getGsmSignalStrength();
		}

	};
	private class ToastRunnable implements Runnable {
	    String mText;
		private Context mContext;

	    public ToastRunnable(Context context, String text) {
	    	mContext = context;
	        mText = text;
	    }

	    @Override
	    public void run(){
	    	try {
	    		Toast.makeText(mContext, mText, Toast.LENGTH_SHORT).show();
	    	}
			catch (Exception e) {
				MapMyTripApp.logToFile(mContext, MapMyTripApp.LOG_TAG_ERROR, "Showing Toast Error:"+e.getMessage());
			}
	    }
	}
	private void shutdownLoggerService() {
		if (locationManager != null) {
			locationManager.removeUpdates(locationListener);
			locationManager = null;
		}

		if (locationListener != null) {
			locationListener = null;
		}
		if (telManager != null) {
			telManager = null;
		}

		if (mSignalListener != null) {
			mSignalListener = null;
		}
		mSensorManager.unregisterListener(mSensorListener);
		MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_INFO, "Location collection service is shutting down...");
		if (timer != null) {
			timer.cancel();
			MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_INFO, "Sensors collection service is shutting down...");
		}
	}
	@SuppressLint("ShowToast")
	private class LocationPostTask extends AsyncTask<List<PointRecord>, Integer, Long> {
		
		private static final String URL_POST = MapTripUtility.baseURL+"postlocation.htm";
		private ArrayList<NameValuePair> nameValuePairs;

		public LocationPostTask() {
			mHandler = new Handler();
		}


		protected Long doInBackground(List<PointRecord>... recs) {
			long id = 0;

			// Creating service handler class instance
			ServiceHandler serviceHandler = new ServiceHandler();
			String result = null;
			int count = recs.length;
			List<PointRecord> list=recs[0];
				try {

					result = serviceHandler.makeServiceCallBulk(URL_POST, ServiceHandler.POST, list);
					String msg="";
					if (result.contains("200")){

						MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_INFO, "Location data posted to Server:"+list.toString());
						for (PointRecord pr:list){
							db.setPointRead(pr.getId());
						}
					}


					if (result.contains("200"))
						msg=list.size()+" Locally Stored Data Posted...";
					else
					if (result.contains("404"))
						msg="Failed to Post Data, Requested resource not found";
					else
					if (result.contains("505"))
						msg="Failed to Post Data,Something went wrong at server end";
					else
					if(result.contains("2000") || result.contains("refused"))
						msg="Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]";
					else
						msg="Failed to Post Data, Unknown Error";

					if (!result.contains("200"))
						MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_ERROR, "Error while Location data post to Server:" + list.toString());

						mHandler.post(new ToastRunnable(GPSLoggerService.getContext(),msg));

				} catch (/* ClientProtocol */Exception e) {
					MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_ERROR, e.getMessage());
				}



			return id;
		}

		protected void onProgressUpdate(Integer... progress) {
		}

		protected void onPostExecute(Long result) {
		}
	}
	public class MyLocationListener implements LocationListener {
		public int mSignal;

		private class MyPhoneStateListener extends PhoneStateListener
	    {
	      /* Get the Signal strength from the provider, each tiome there is an update */
	      @Override
	      public void onSignalStrengthsChanged(SignalStrength signalStrength)
	      {
	         super.onSignalStrengthsChanged(signalStrength);
	         mSignal = signalStrength.getGsmSignalStrength();
	      }

	    };
		public void onLocationChanged(Location loc) {
//			MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_INFO, "Location Changed...");
			firstTime = false;
			if (loc != null) {
				lastKnownLocation = loc;
			} else
				MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_INFO, "Location not found...");
		}
		

		public void onProviderDisabled(String provider) {
			try {
				MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_INFO, "provider is about to disabled...");
				if (showingDebugToast && provider != null) 
					Toast.makeText(GPSLoggerService.getContext(), "onProviderDisabled: " + provider, Toast.LENGTH_SHORT).show();
				if (!firstTime)
					checkGPSStatus();
			}catch (Exception e) {
				
			}
		}

		public void onProviderEnabled(String provider) {
			try {
				if (locationListener == null) {
					locationListener = new MyLocationListener();
				}
				if (locationManager == null) {
					locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
					locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTimeMillis, 0, locationListener);
				}

				MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_INFO, "provider is about to enabled...");
				if (showingDebugToast && provider != null)
					Toast.makeText(GPSLoggerService.getContext(), "onProviderEnabled: " + provider, Toast.LENGTH_SHORT).show();
			} catch (Exception e) {

			}

		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
			try {
				String showStatus = null;
				if (status == LocationProvider.AVAILABLE)
					showStatus = "Available";
				if (status == LocationProvider.TEMPORARILY_UNAVAILABLE)
					showStatus = "Temporarily Unavailable";
				if (status == LocationProvider.OUT_OF_SERVICE)
					showStatus = "Out of Service";
				if (status != lastStatus && showingDebugToast) {
					Toast.makeText(GPSLoggerService.getContext(), "new status: " + showStatus, Toast.LENGTH_SHORT).show();
				}
				lastStatus = status;
			}catch (Exception e) {
				
			}

		}

	}

	// service framework methods

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public void onDestroy() {



		if (candelLocalDataCollectionTask()){
			Toast.makeText(this, "Data Collection Task Canceled",
					Toast.LENGTH_SHORT).show();
		}
		if (candelPostDataTimerTask()){
			Toast.makeText(this, "Data Post Task Canceled",
					Toast.LENGTH_SHORT).show();
		}


		super.onDestroy();
		
		shutdownLoggerService();
		
		// Cancel the persistent notification.
		mNM.cancel(R.string.local_service_started);

		// Tell the user we stopped.
		Toast.makeText(this, R.string.local_service_stopped,
						Toast.LENGTH_SHORT).show();
	}

	/**
	 * Show a notification while this service is running.
	 */
	private void showNotification() {
		// In this sample, we'll use the same text for the ticker and the
		// expanded notification
		CharSequence text = getText(R.string.local_service_started);

		// Set the icon, scrolling text and timestamp
		Notification notification = new Notification(R.drawable.ic_launcher,
				text, System.currentTimeMillis());

		// The PendingIntent to launch our activity if the user selects this
		// notification
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				new Intent(this, GPSLoggerService.class), 0);
		
		// Set the info for the views that show in the notification panel.
		notification.setLatestEventInfo(this, getText(R.string.service_name),
				text, contentIntent);


		// Send the notification.
		// We use a layout id because it is a unique number. We use it later to
		// cancel.
		mNM.notify(R.string.local_service_started, notification);
	}

	

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	public static void setMinTimeMillis(long _minTimeMillis) {
		minTimeMillis = _minTimeMillis;
	}

	public static long getMinTimeMillis() {
		return minTimeMillis;
	}

	public static void setMinDistanceMeters(long _minDistanceMeters) {
		minDistanceMeters = _minDistanceMeters;
	}

	public static long getMinDistanceMeters() {
		return minDistanceMeters;
	}

	public static float getMinAccuracyMeters() {
		return minAccuracyMeters;
	}
	
	public static void setMinAccuracyMeters(float minAccuracyMeters) {
		GPSLoggerService.minAccuracyMeters = minAccuracyMeters;
	}

	public static void setShowingDebugToast(boolean showingDebugToast) {
		GPSLoggerService.showingDebugToast = showingDebugToast;
	}

	public static boolean isShowingDebugToast() {
		return showingDebugToast;
	}

	/**
	 * Class for clients to access. Because we know this service always runs in
	 * the same process as its clients, we don't need to deal with IPC.
	 */
	public class LocalBinder extends Binder {
		public GPSLoggerService getService() {
			return GPSLoggerService.this;
		}
	}
	
	SensorEventListener mSensorListener = new SensorEventListener() {
		
		@Override
		public void onSensorChanged(SensorEvent event) {
			Sensor mySensor = event.sensor;
			if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
		        accX = event.values[0];
		        accY = event.values[1];
		        accZ = event.values[2];
		    } else if (mySensor.getType() == Sensor.TYPE_GYROSCOPE) {
		        gyrX = event.values[0];
		        gyrY = event.values[1];
		        gyrZ = event.values[2];
		    } else if (mySensor.getType() == Sensor.TYPE_LIGHT) {
		        light = event.values[0];
		    } else if (mySensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
		        magX = event.values[0];
		        magY = event.values[1];
		        magZ = event.values[2];
		    }
			
		}
		
		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			
		}
	};
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		int result=super.onStartCommand(intent, flags, startId);
		//Bundle b=intent.getExtras();
		//driverId=b.getLong("driverId");
		MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_INFO, "Location collection service about to start...");
		mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		startLoggerService();

		// Display a notification about us starting. We put an icon in the
		// status bar.
		showNotification();

		return result;
	}

	public void startInitialTasks(){
		MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_INFO, "Location collection service about to start...");
		mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		startLoggerService();

		// Display a notification about us starting. We put an icon in the
		// status bar.
		showNotification();

	}

	public void setDriverId(Long driverId){

		this.driverId=driverId;
	}


	public synchronized void setupTimer(long duration){
		if(timerLocationDataCollector != null) {
			timerLocationDataCollector.cancel();
			timerLocationDataCollector = null;
		}
		timerLocationDataCollector = new Timer();
        if (timerTask!=null){
            timerTask.cancel();
            timerTask=null;
        }
        timerTask=new TimerTask() {
			private int counting = 0;
			TimeZone tz = TimeZone.getTimeZone("GMT+03:00");
			Calendar c = Calendar.getInstance(tz);

			public void run() {

//				MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_INFO, "Location Changed...");
				firstTime = false;
				if (locationManager == null)
					locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

				Location locGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				Location locNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				Location locPassive = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

				List<Location> listLOCs = new ArrayList<Location>();
				Location loc = null;

				if (locGPS != null) {
					listLOCs.add(locGPS);
				}
				if (locNetwork != null) {
					listLOCs.add(locNetwork);
				}
				if (locPassive != null) {
					listLOCs.add(locPassive);
				}

				if (listLOCs.size() > 0) loc = listLOCs.get(0);

				//get the latest location by timestasmp
				for (Location tLoc : listLOCs) {
					Log.d("Locaton from", tLoc.getProvider() + " " + MapTripUtility.timestampFormat.format(tLoc.getTime()));
					if (loc.getTime() < tLoc.getTime())
						loc = tLoc;
				}
				try {
					Log.d("Latest locaton", loc.getProvider() + " " + MapTripUtility.timestampFormat.format(loc.getTime()));
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (loc == null && lastKnownLocation != null)
					loc = lastKnownLocation;

				if (loc != null) {
					//boolean pointIsRecorded = false;
					int tempTripIndex = 0;
					lastKnownLocation = loc;
					try {
//						if (loc.hasAccuracy() && loc.getAccuracy() <= minAccuracyMeters) {

						tempTripIndex = MapMyTripApp.tripCounter;

						final PointRecord record = new PointRecord();
						final List <PointRecord>recordList =new ArrayList<PointRecord>();
						SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(GPSLoggerService.getContext());
						String imei = sharedPreferences.getString(MapMyTripApp.wifiKey, "0");
						if (imei == "0" || !imei.contains(":")) {
							MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_INFO, "About to Register New Device while getting new location...");
							new VehicleRegistrationTask(GPSLoggerService.getContext()).execute();
						}
						record.setUid(imei);
						record.setId(tempTripIndex);
						record.setTimeStamp(MapTripUtility.timestampFormat.format(c.getTimeInMillis()));
						record.setPostTimeStamp(MapTripUtility.timestampFormat.format(c.getTimeInMillis()));
						record.setLatitude(loc.getLatitude());
						record.setLongitude(loc.getLongitude());

						record.setDriverId(driverId);

						if (loc.hasAltitude())
							record.setAltitude(loc.getAltitude());
						else
							record.setAltitude(0.0);

						if (loc.hasAccuracy())
							record.setAccuracy((double) loc.getAccuracy());
						else
							record.setAccuracy(0.0);

						if (loc.hasSpeed())
							record.setSpeed((double) loc.getSpeed());
						else
							record.setSpeed(0.0);

						if (loc.hasBearing())
							record.setBearing((double) loc.getBearing());
						else
							record.setBearing(0.0);

						if (accX == -1)
							record.setAccX(accX);
						else
							record.setAccZ(0);
						if (accY == -1)
							record.setAccY(accY);
						else
							record.setAccY(0);
						if (accZ == -1)
							record.setAccZ(accZ);
						else
							record.setAccZ(0);

						if (gyrX == -1)
							record.setGyrX(gyrX);
						else
							record.setGyrX(0);
						if (gyrY == -1)
							record.setGyrY(gyrY);
						else
							record.setGyrY(0);
						if (gyrZ == -1)
							record.setGyrZ(gyrZ);
						else
							record.setGyrZ(0);

						if (magX == -1)
							record.setMagX(magX);
						else
							record.setMagX(0);
						if (magY == -1)
							record.setMagY(magY);
						else
							record.setMagY(0);
						if (magZ == -1)
							record.setMagZ(magZ);
						else
							record.setMagZ(0);

						if (light == -1)
							record.setLight(light);
						else
							record.setLight(0);
						record.setNetworkType(get_network());
						record.setOperatorName(getOperatorName());
						record.setSignalStrength(GPSLoggerService.mSignal);

						db.insertPointRecord(tempTripIndex, record, MapTripDatabase.RECORD_UNREAD);
						MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_INFO, "Storing record in local database...");
						counting++;
						if (counting>10) {
							counting = 0;
							mHandler.post(new ToastRunnable(GPSLoggerService.getContext(), "Location Data, Storing Locally..."));
						}


						// here we will send data to server.
						Intent intent = new Intent();
						intent.setAction(MapTripUtility.PositionCoordinates);
						intent.putExtra("Latitude", record.getLatitude());
						intent.putExtra("Longitude", record.getLongitude());
						intent.putExtra("TimeStamp", record.getTimeStamp());
						sendBroadcast(intent);
//						} else {
//							MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_INFO, "Location is not Accurate, so, we will not use that location...");
//						}
					} catch (Exception e) {
						MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_ERROR, e.getMessage());
						Log.e(tag, e.toString());
					}
				} else
					MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_INFO, "Location not found...");

			}
		};

		timerLocationDataCollector.scheduleAtFixedRate(timerTask, 0, duration);
	}
	LocationPostTask locationPostTask;
	public synchronized void setupTimer2(long duration) {
		if (timerLocationDataSender != null) {
			timerLocationDataSender.cancel();
			timerLocationDataSender = null;
		}
		timerLocationDataSender = new Timer();
        if(postDataTimerTask!=null){
            postDataTimerTask.cancel();
            postDataTimerTask=null;
        }
			postDataTimerTask = new TimerTask() {
				TimeZone tz = TimeZone.getTimeZone("GMT+03:00");
				Calendar c = Calendar.getInstance(tz);

			public void run() {
				try {
					final List<PointRecord>pointRecords=new ArrayList<PointRecord>();

					if (isOnline()) {
						MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_INFO, "User is online while posting location...");
						//int locallyStoredCount = db.getUnreadPositionDetail().size();
						for (final PointRecord rec : db.getUnreadPositionDetail()) {
							rec.setPostTimeStamp(MapTripUtility.timestampFormat.format(c.getTimeInMillis()));
							pointRecords.add(rec);
						}
						MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_INFO, "Location information about to Post...");
						Handler h = new Handler(Looper.getMainLooper());
						h.post(new Runnable() {
							public void run() {
                                if (locationPostTask!=null) {
                                    locationPostTask.cancel(true);
                                    locationPostTask = null;
                                }
								locationPostTask=new LocationPostTask();
								locationPostTask.execute(pointRecords);
							}
						});

					}

				} catch (Exception e) {
					MapMyTripApp.logToFile(GPSLoggerService.getContext(), MapMyTripApp.LOG_TAG_ERROR, e.getMessage());
					Log.e(tag, e.toString());
				}
			}


		};


			timerLocationDataSender.scheduleAtFixedRate(postDataTimerTask,0,duration);
		}
	public boolean candelPostDataTimerTask(){
		boolean result=true;
		try {
			timerLocationDataSender.cancel();
			timerLocationDataSender=null;
			postDataTimerTask.cancel();
			postDataTimerTask=null;
			locationPostTask.cancel(true);
			locationPostTask=null;

		}catch (Exception e){
			result=false;
		}
		return result;
	}
	public boolean candelLocalDataCollectionTask(){
		boolean result=true;
		try {
			timerLocationDataCollector.cancel();
			timerLocationDataCollector=null;
			timerTask.cancel();
			timerTask=null;
		}catch (Exception e){
			result=false;
		}
		return result;
	}

	}


