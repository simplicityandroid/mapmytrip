package com.uqu.mapmytrip;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

class VehicleRegistrationTask extends AsyncTask<URL, Integer, Long> {
	private static final String URL_GET_ID = "http://54.172.33.121:8080/getNextDeviceId.htm";
	private static final String URL_REG = "http://54.172.33.121:8080/register_user.htm";
	private List<NameValuePair> nameValuePairs;
	private Context mContext;

	public VehicleRegistrationTask(Context context) {
		mContext = context;
	}

	protected Long doInBackground(URL... urls) {
		long id = -1;

		// Creating service handler class instance
		ServiceHandler serviceHandler = new ServiceHandler();
        
		String deviceId = serviceHandler.makeServiceCall(URL_GET_ID, ServiceHandler.GET, null);
		String myDeviceModel = android.os.Build.MODEL;
		String myDeviceBrand = android.os.Build.BRAND;
		WifiManager wifi = (WifiManager)mContext.getSystemService(Context.WIFI_SERVICE);
		String unique = null;
		if (wifi.getConnectionInfo() != null && wifi.getConnectionInfo().getMacAddress() != null)
			unique = wifi.getConnectionInfo().getMacAddress();
    	else
    		unique = android.os.Build.SERIAL+":1";
		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		String name = "UNKNOWN";
		if (MapMyTripApp.deviceName != null)
			name = MapMyTripApp.deviceName;
		else if (mBluetoothAdapter != null)
        	name = mBluetoothAdapter.getName();
		String regStr = null;
		if (deviceId == null)
			return id;
		try {
			// Add the data
//			String uniqueId = deviceId + "-" + imei;
			id = Long.valueOf(deviceId);
			nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("id", deviceId));
			nameValuePairs.add(new BasicNameValuePair("name", name));
			nameValuePairs.add(new BasicNameValuePair("brand", myDeviceBrand+ " " + myDeviceModel));
			nameValuePairs.add(new BasicNameValuePair("imei", unique));
			MapTripDatabase db = MapTripDatabase.getInstance();
			if (db == null) {
				MapTripDatabase.init(mContext);
				db = MapTripDatabase.getInstance();
			}
			db.getLastInsertId();
			regStr = serviceHandler.makeServiceCall(URL_REG, ServiceHandler.POST, nameValuePairs);
			if (regStr == null || regStr.equals("") || regStr.equals("-1"))
				MapMyTripApp.logToFile(mContext, MapMyTripApp.LOG_TAG_ERROR, "Failed to register New Device...");
			else {
				MapMyTripApp.logToFile(mContext, MapMyTripApp.LOG_TAG_INFO, "Device registered:" + regStr);
				SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
				Editor editor = sharedPreferences.edit();
	        	editor.putBoolean(MapMyTripApp.mapRecordKey, true);
	        	editor.putString(MapMyTripApp.wifiKey, unique);
	        	editor.commit();
			}
		} catch (/* ClientProtocol */Exception e) {
			MapMyTripApp.logToFile(mContext, MapMyTripApp.LOG_TAG_ERROR, e.getMessage());
		}

		return id;
	}

	protected void onProgressUpdate(Integer... progress) {
	}

	protected void onPostExecute(Long result) {
	}
}
