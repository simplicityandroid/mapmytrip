package com.uqu.mapmytrip;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.simplicity.io.mapmytrip.R;
import com.uqu.camerarecorder.BackgroundVideoRecorder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


/**
 * This shows how to create a simple activity with a map and a marker on the map.
 * <p>
 * Notice how we deal with the possibility that the Google Play services APK is not
 * installed/enabled/updated on a user's device.
 */
public class MapMyTripApp extends FragmentActivity implements
												ConnectionCallbacks,
												/*OnConnectionFailedListener,*/
												ActionBar.OnNavigationListener
{

	/**
	 * Called when the activity is first created.
	 */
	private Intent intent;
	private Intent intentGPSLoggerService;
	private BackgroundVideoRecorder mService;
	private GPSLoggerService mServiceGPSLogger;
	private boolean mBound;
	private boolean mBoundGPSLogger;
	private long interval=5;

	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected (ComponentName name, IBinder iBinder){
			BackgroundVideoRecorder.LocalBinder binder = (BackgroundVideoRecorder.LocalBinder) iBinder;
			mService = binder.getService();
			mBound = true;
			mService.setTripName(currentTripName);
		}
		@Override
		public void onServiceDisconnected (ComponentName name){
			mBound = false;

		}
	};

	private ServiceConnection mConnectionGPSLogger = new ServiceConnection() {

		@Override
		public void onServiceConnected (ComponentName name, IBinder iBinder){
			GPSLoggerService.LocalBinder binder = (GPSLoggerService.LocalBinder) iBinder;
			mServiceGPSLogger = binder.getService();
			mBoundGPSLogger = true;
			mServiceGPSLogger.startInitialTasks();
			mServiceGPSLogger.setDriverId(driverId);
			mServiceGPSLogger.setupTimer(interval*1000);
			mServiceGPSLogger.setupTimer2(30000);
		}
		@Override
		public void onServiceDisconnected (ComponentName name){

			mBoundGPSLogger = false;

		}
	};



	/**
     * Note that this may be null if the Google Play services APK is not available.
     */
    private GoogleMap mMap;
    private GroundOverlay mGroundOverlay;
    
    private LocationClient mLocationClient;
    private ActionBar actionBar;
    private	Location currentLocation;
 
    private String currentTripName = "";
    private static final String tag = "MapMyTripTag";
    Boolean isMapRecordingStarted = false;

    private PositionReceiver positionReceiver;
   
    // Title navigation Spinner data
 	private ArrayList<SpinnerNavItem> navSpinner;
 
 	// Navigation adapter
 	private TitleNavigationAdapter adapter;
 	
 	private MapTripDatabase db = null;
    
    public SharedPreferences sharedPreferences;
    public static final String mapRecordKey = "MAPRECORDKEY";
    public static final String currentTripKey = "CURRENTTRIP";
    public static final String tripCounterKey = "TRIPNAMECOUNTERKEY";
    public static final String wifiKey = "WIFIKEY";
    
    public static int tripCounter = 0;
    public static String currentTrip = "trip_";
    
    private static int menuState = 0;

    private IntentFilter positionIntentFilter;
    private LocationManager locationManager;
    
	public static String LOG_TAG_ERROR = "Error";
	public static String LOG_TAG_WARNING = "Warning";
	public static String LOG_TAG_VERBOSE = "Verbose";
	public static String LOG_TAG_INFO = "Info";
	public static String LOG_TAG_DEBUG = "Debug";

	public static String deviceName = null;
	public long driverId;

	NetworkChangeReceiver networkChangeReceiver;

	String[] intervals = {"5", "10", "15","20", "30","40", "50","60"};

    public Boolean getIsMapRecordingStarted() {
		return isMapRecordingStarted;
	}
	
	public void setIsMapRecordingStarted(Boolean mapRecordingFlag) {
		isMapRecordingStarted = mapRecordingFlag;
	}
	
	public int	getTripCounter() {	
		return tripCounter;	
	}
	
	public static void setTripCounter(int tripCount) {
		tripCounter = tripCount;
	}

	
	@Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.maptrip);

		Toast.makeText(getApplicationContext(),NetworkUtil.getConnectivityStatusString(getApplicationContext()),Toast.LENGTH_LONG).show();

		intent=new Intent(this,BackgroundVideoRecorder.class);
		intentGPSLoggerService = new Intent(this,GPSLoggerService.class);

		try {

			driverId = getIntent().getExtras().getLong("driverId");

		}catch(Exception e){}

        
        //initialize database
      	db = getDB();
        // Start Location Collection Service on App Load...
      	sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
      	String deviceUniqueId = sharedPreferences.getString(MapMyTripApp.wifiKey, "0");
        checkGPSStatus();
        registerDevice(this, deviceUniqueId);

        setUpMapIfNeeded();
    	if (mMap != null) {
    		mMap.clear();
    		currentLocation = mMap.getMyLocation();
    	}
    	positionReceiver = new PositionReceiver();
	    positionIntentFilter = new IntentFilter();
	    positionIntentFilter.addAction(MapTripUtility.PositionCoordinates);
	    registerReceiver(positionReceiver, positionIntentFilter);

		networkChangeReceiver=new NetworkChangeReceiver();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
		//intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
		//intentFilter.addAction("android.net.wifi.STATE_CHANGE");
		registerReceiver(networkChangeReceiver,intentFilter);



		getAppSharedState();
	    this.isMapRecordingStarted = true;
	    menuState = 1;

	    startNewTrip();
		startGPSLoggerService();
		startVideoRecordingService();
		saveAppSharedState();
		if (currentLocation != null) {
			// addMarker(currentLocation, true);
			addMarker(currentLocation.getLatitude(), currentLocation.getLongitude(), 
					MapTripUtility.timestampFormat.format(currentLocation.getTime()),
					MapTripUtility.routeStartFlag);
			if (mMap != null) {
				LatLng currentlLatLon = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
				mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentlLatLon, 15));
			}

		    // Zoom in, animating the camera.
//			mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);

		}
		this.invalidateOptionsMenu();
    	
    	
        Log.i(tag, "onCreate called.");
        MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_INFO, "App Loaded...");
        
        
        actionBar = getActionBar();

		// Hide the action bar title
		actionBar.setDisplayShowTitleEnabled(false);

		// Enabling Spinner dropdown navigation
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		
		actionBar.setDisplayHomeAsUpEnabled(false);
		
		// Spinner title navigation data
		navSpinner = new ArrayList<SpinnerNavItem>();
		
		navSpinner.add(new SpinnerNavItem("MapMyTrip", R.drawable.ic_launcher));
		navSpinner.add(new SpinnerNavItem("Home", R.drawable.ic_home));
		navSpinner.add(new SpinnerNavItem("Tawaaf", R.drawable.ic_tawaaf));
		navSpinner.add(new SpinnerNavItem("History", R.drawable.ic_history));
		navSpinner.add(new SpinnerNavItem("Help", R.drawable.ic_help));
		//navSpinner.add(new SpinnerNavItem("Latitude", R.drawable.ic_latitude));

		// title drop down adapter
		adapter = new TitleNavigationAdapter(getApplicationContext(), navSpinner);

		// assigning the spinner navigation
		actionBar.setListNavigationCallbacks(adapter, this);
		
		
		if (mMap != null)
			currentLocation = mMap.getMyLocation();
		
//		if(this.isMapRecordingStarted) {
//			menuState = 1;
//			boolean isServiceRunning = isMyServiceRunning(GPSLoggerService.class);
//			if (!isServiceRunning)
//				startService(new Intent(MapMyTripApp.this, GPSLoggerService.class));
//		}
		
		
//			boolean isServiceRunning = isMyServiceRunning(GPSLoggerService.class);
//			if (!isServiceRunning)
//				startService(new Intent(MapMyTripApp.this, GPSLoggerService.class));
//		}
//		invalidateOptionsMenu();		
    }
	private void registerDevice(final Context context, final String deviceUniqueId) {
		// get prompts.xml view

		if (deviceUniqueId == "0" || !deviceUniqueId.contains(":")) {
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_INFO, "About to Register New Device on app load...");
			new VehicleRegistrationTask(getApplicationContext()).execute();
		}
		else if (deviceName != null) {
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_INFO, "Device already Registered but User Provided different Device Name...");
			new VehicleNameUpdateTask(getApplicationContext()).execute();
		}


}

	// Check Wi-Fi is on
	boolean confirmWiFiAvailable(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		return wifiInfo.isAvailable();
	}
	// Check Wi-Fi is on
	boolean confirmGPRSAvailable(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo gprsInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (gprsInfo != null)
			return gprsInfo.isAvailable();
		return false;
	}

	// Check Airplane Mode - we want airplane mode off
	boolean confirmAirplaneModeOff(Context context) {
		int airplaneSetting = Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0);
		return airplaneSetting == 0;
	}

	boolean isNetLocUsable(Context context) {
		return (confirmGPRSAvailable(context) || confirmWiFiAvailable(context)) && confirmAirplaneModeOff(context);
	}
	private boolean checkGPSStatus() {
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		boolean status = true;
		/*
		 * try to see if the location stored on android is close enough for
		 * you,and avoid re-requesting the location
		 */
		if (!isNetLocUsable(this)) {
			try {
				displayPromptForEnablingNetwork(this);
			}
			catch (Exception e) {
				MapMyTripApp.logToFile(getApplicationContext(), MapMyTripApp.LOG_TAG_ERROR, "Show Network Dialog Error:"+e.getMessage());
			}
			MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_ERROR, "Internet is on app load Disabled...");
			status = false;
		} else 
			MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_INFO, "Internet is Ok on app load...");
		boolean gpsLocation = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

		if (!gpsLocation) {
			try {
				displayPromptForEnablingGPS(this);
			}
			catch (Exception e) {
				MapMyTripApp.logToFile(getApplicationContext(), MapMyTripApp.LOG_TAG_ERROR, "Show GPS Dialog Error:"+e.getMessage());
			}
			MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_ERROR, "GPS is Disabled on app load...");
			status = false;

		} else {
			MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_INFO, "GPS is Ok on app load...");
		}
		
		return status;
	}
    
    public static void displayPromptForEnablingGPS(
            final Activity activity)
        {
            final AlertDialog.Builder builder =
                new AlertDialog.Builder(activity);
            final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
            final String message = "Enable GPS service to collect locations information"
					+ " Click Settings to go to GPS settings."
					+ " Application will not perform correctly in the absence of GPS";
     
            builder.setMessage(message)
                .setPositiveButton("Settings",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface d, int id) {
								activity.startActivity(new Intent(action));
								d.dismiss();
							}
						})
                .setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface d, int id) {
								d.cancel();
							}
						});
            
            builder.create().show();
        }

	public static void displayPromptForEnablingNetwork(final Activity activity) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		final String action = Settings.ACTION_WIRELESS_SETTINGS;
		final String message = "Enable Wifi/Cellular Data to Sync location data with Server"
				+ " Click Settings to go to Wifi settings."
				+ " Application will not perform correctly in the absence of Network";

		builder.setMessage(message)
				.setPositiveButton("Settings",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface d, int id) {
								activity.startActivity(new Intent(action));
								d.dismiss();
							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface d, int id) {
								d.cancel();
							}
						});

		builder.create().show();
	}

    /* Function : getAppSharedState */
    private void getAppSharedState() {
    	sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        
        this.isMapRecordingStarted = sharedPreferences.getBoolean(mapRecordKey, false);
        currentTripName = sharedPreferences.getString(currentTripKey, currentTrip);
        tripCounter = sharedPreferences.getInt(tripCounterKey, 0);
    }
    
    /* Function : saveAppSharedState */
    private void saveAppSharedState() {
    	if(sharedPreferences != null) {
        	Editor editor = sharedPreferences.edit();
        	editor.putBoolean(mapRecordKey, this.isMapRecordingStarted);
        	editor.putString(currentTripKey, currentTripName);
        	editor.putInt(tripCounterKey, tripCounter);
        	editor.commit();
        }
    }

    @Override
    protected void onResume() {
		try {
			super.onResume();
			getAppSharedState();
			// Log.i(tag, "onResume::Map Recording status: "+
			// isMapRecordingStarted.toString());
			MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_INFO, "MapMyTrip Resumed...");

			setUpMapIfNeeded();

			// Log.i(tag, "onResume::");

			if (isMapRecordingStarted) {
				menuState = 1;
				renderLocationList(tripCounter);
			}

			int tempId = MapTripUtility.getRouteId();
			if (tempId != -1) {
				MapTripUtility.setRouteId(-1);
				renderLocationList(tempId);
			}

			actionBar.setSelectedNavigationItem(0);
		} catch (Exception e) {
			MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_ERROR, "Exception while resuming:"+ e.getMessage());
		}
    }
    
    @Override
	public void onPause() {
		// Log.i(tag, "onPause::");
		try {
			MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_INFO, "MapMyTrip Paused...");

			// if (mLocationClient != null) {
			// mLocationClient.disconnect();
			// }

			saveAppSharedState();
		} catch (Exception e) {
			MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_ERROR, "Exception while pausings:"+ e.getMessage());
		}
		super.onPause();

	}
    
    public void renderLocationList(int tripId) {
		// Get the DB contents
    	
    	if(mMap != null) {
    		mMap.clear();
    		currentLocation = mMap.getMyLocation();
    	}
    	
    	new AsyncTask<Object, Void, Cursor>() {
			
			@Override
			protected Cursor doInBackground(Object... params) {
				int id = (Integer)params[0];
				
				//Log.i(tag, "Going to render locationList with tripId = " + id);

				return db.getRoute(id);
			}
			
			@Override
			protected void onPostExecute(Cursor cursor) {
				
				int id;
				String timeStamp = null;
				double latitude = 0.0;
				double longitude = 0.0;
				double altitude = 0.0;
				double accuracy = 0.0;
				if (cursor != null) {
	    			
				try {
					//Log.i(tag, "locationList:: cursor->count=" + cursor.getCount());
					int _idIndex = cursor.getColumnIndexOrThrow(MapTripDatabase.PointsColumn.ID);
					int gmtTimestampColumnIndex = cursor.getColumnIndexOrThrow(MapTripDatabase.PointsColumn.TIMESTAMP);
					int latitudeColumnIndex = cursor.getColumnIndexOrThrow(MapTripDatabase.PointsColumn.LATITUDE);
		            int longitudeColumnIndex = cursor.getColumnIndexOrThrow(MapTripDatabase.PointsColumn.LONGITUDE);
		            int altitudeColumnIndex = cursor.getColumnIndexOrThrow(MapTripDatabase.PointsColumn.ALTITUDE);
		            int accuracyColumnIndex = cursor.getColumnIndexOrThrow(MapTripDatabase.PointsColumn.ACCURACY);
		            int speedColumnIndex = cursor.getColumnIndexOrThrow(MapTripDatabase.PointsColumn.SPEED);
		            int bearingColumnIndex = cursor.getColumnIndexOrThrow(MapTripDatabase.PointsColumn.BEARING);
		            
	                if (cursor.moveToFirst()) {
	    				
	    				Boolean firstPointFlag = true;
	    				
	    				do {
	    					
	    					
	    					id = cursor.getInt(_idIndex);
	    					timeStamp = cursor.getString(gmtTimestampColumnIndex);
	    					latitude = cursor.getDouble(latitudeColumnIndex);
	    					longitude = cursor.getDouble(longitudeColumnIndex);
	    					altitude = cursor.getDouble(altitudeColumnIndex);	// + altitudeCorrectionMeters;
	    					accuracy = cursor.getDouble(accuracyColumnIndex);
	    					
	    					/*//
	    					Log.i(tag, "renderLocationList:" + 
	    								Integer.toString(id) + "," + timeStamp + ", " +
	    								Double.toString(latitude) + "," + Double.toString(longitude) );//*/
	    					
	    					if(firstPointFlag) {
	    						addMarker(latitude, longitude, timeStamp, MapTripUtility.routeStartFlag);
	    						gotoMyLocation(latitude, longitude);
	    						firstPointFlag = false;
	    					}
	    					
	    					
	    					if(mMap != null) {
		    					addMarker(latitude, longitude, timeStamp, MapTripUtility.routePointFlag);
	    					}
	    					
	    					if(currentLocation == null) {
	    						currentLocation = mMap.getMyLocation();
	    					}
	    					else {
	    						currentLocation.setLatitude(latitude);
	    						currentLocation.setLongitude(longitude);
	    					}
	    				} while (cursor.moveToNext());
	    				
	        			Toast.makeText(getBaseContext(), "rendering completed!", Toast.LENGTH_LONG).show();
	    			} else {
	    				Toast.makeText(getBaseContext(),
	    						"[INFO] No data points record available yet.",
	    						Toast.LENGTH_LONG).show();
	    			}
	    		} catch (Exception e) {
	    			MapMyTripApp.logToFile(getApplicationContext(), MapMyTripApp.LOG_TAG_ERROR, "Exception while rendering location list:"+e.getMessage());
	    			Toast.makeText(getBaseContext(),
	    					"[ERROR] DB errror during rendering: " + e.getMessage(),
	    					Toast.LENGTH_LONG).show();
	    		} finally {
	    			if (cursor != null && !cursor.isClosed()) {
	    				cursor.close();
	    			}
	    			
	    			// Mark last marker
	    			if(!isMapRecordingStarted && (currentLocation != null)) {
	    				addMarker(currentLocation.getLatitude(), 
	    						currentLocation.getLongitude(),
	    						MapTripUtility.timestampFormat.format(currentLocation.getTime()),
	    						MapTripUtility.routeEndFlag);
	    				gotoMyLocation(currentLocation.getLatitude(), currentLocation.getLongitude());
	    			}
	    		}
				}
			}
		}.execute(tripId);
		
    }//end renderLocationList
    	
    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_INFO, "Map initialized...");
			Toast.makeText(this, "Google Map initialized...", Toast.LENGTH_LONG).show();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                //setUpMap();
                mMap.setMyLocationEnabled(true);
                //mMap.setOnMyLocationButtonClickListener(this);	//TH! Location Button listener
            }
            else {
            	MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_ERROR, "Map not available...");
            	Toast.makeText(this, "Google Maps not available", Toast.LENGTH_LONG).show();
            }
        }
    }
    
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.map_my_hajj_app, menu);
        //return true;
    	
    	//*// Verssion 2
    	MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.map_my_trip_app, menu);
		
		MenuItem item;
		
		switch(menuState) 
		{
			case 0:
				item = menu.findItem(R.id.action_stop);
				item.setVisible(false);
				break;
			case 1:
				item = menu.findItem(R.id.action_start);
				item.setVisible(false);
				break;
				
			default:
				return super.onCreateOptionsMenu(menu);
		}
		
		return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
 
        switch (item.getItemId()) {
 
        case R.id.menu_sethybrid:
        	if(mMap != null)
        		mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            break;
            
        case android.R.id.home:
			//Log.i(tag, "Android home clicked.");
            break;
        /*    
        case R.id.menu_clearall:
        	setTripCounter(0);
    
        	new AsyncTask<Void, Void, Integer>() {

				@Override
				protected Integer doInBackground(Void... params) {
					Cursor c = db.getRouteCount();
					
					if(c != null) {
						for(int i=1; i<= c.getCount(); i++) {
							db.deleteTrip(i);
						}
					}
					
					return 0;
				}
				
				@Override
				protected void onPostExecute(Integer temp) {
					Editor editor = sharedPreferences.edit();
		    		editor.clear();
		    		editor.commit();
				}
        		
        	}.execute();
        	
        	break;
	        */
        // GO to my location    
        case R.id.menu_normalmap:
        	if(mMap != null)
        		mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        break;
        
        case R.id.action_stop:
        	//Log.i(tag, "Stop action with counter = " + Integer.toString(tripCounter));
    		
        	long time = Calendar.getInstance().getTimeInMillis();
        	
        	if (currentLocation != null) {
				
        		addMarker(currentLocation.getLatitude(), 
        					currentLocation.getLongitude(),
        					MapTripUtility.timestampFormat.format(currentLocation.getTime()),
        					MapTripUtility.routeEndFlag);
        	}
        	
        	if(this.isMapRecordingStarted) { 
    			Toast.makeText(this, "Trip map routing is going to stop", Toast.LENGTH_LONG).show();
    			menuState = 0;
    			this.invalidateOptionsMenu();
    		}
    		else {
    			Toast.makeText(this, "No trip is in progress.", Toast.LENGTH_LONG).show();
    		}
    		
    		this.isMapRecordingStarted = false;
    		if (mMap.getMyLocation() != null) {
    			try {
    				gotoMyLocation(mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude());
    			} catch (Exception e) {
    				MapMyTripApp.logToFile(getApplicationContext(), MapMyTripApp.LOG_TAG_ERROR, "Exception while goto My Location:"+e.getMessage());
    			}
    		}
    		
    		try {
    			unregisterReceiver(positionReceiver);
    		}catch (Exception e) {
    			//Log.e(tag, "Failed to unregister position receiver");
    			MapMyTripApp.logToFile(getApplicationContext(), MapMyTripApp.LOG_TAG_ERROR, "Exception while unreg position receiver:"+e.getMessage());
    		}

			try {
				unregisterReceiver(networkChangeReceiver);
			}catch (Exception e) {
				//Log.e(tag, "Failed to unregister position receiver");
				MapMyTripApp.logToFile(getApplicationContext(), MapMyTripApp.LOG_TAG_ERROR, "Exception while unreg network change receiver:"+e.getMessage());
			}

    		//stopService(new Intent(MapMyTripApp.this, GPSLoggerService.class));
			stopGPSLoggerService();
			stopVideoRecordingService();


			break;
    	
        case R.id.action_about:
        	Intent intent = new Intent(MapMyTripApp.this, InfoActivity.class);
    		startActivity(intent);
        	break;
        case R.id.action_email:
        	SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			String deviceUniqueId = sharedPreference.getString(MapMyTripApp.wifiKey, "0");
			
			final Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        	emailIntent.setType("plain/text");
        	emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] { "simplicitylabs@gmail.com" });
        	emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"Sensors & Positions data of "+deviceUniqueId);
        	
			File positionFile = new File(Environment.getExternalStorageDirectory(), "MapMyTrip/MapMyTrip_"+deviceUniqueId.replaceAll(":", "")+".sql");
			File sensorFile = new File(Environment.getExternalStorageDirectory(), "MapMyTrip/MapMyTripSensor_"+deviceUniqueId.replaceAll(":", "")+".sql");
			ArrayList<Uri> uris = new ArrayList<Uri>();
			uris.add(Uri.fromFile(positionFile));
			uris.add(Uri.fromFile(sensorFile));
//       		emailIntent.putExtra(Intent.EXTRA_STREAM, uris);
//       		emailIntent.putExtra(Intent.EXTRA_STREAM, uri2);
       		emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        	emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "There are 2 files, 1 containing Sensors information SQL Script and 2nd containing Positions information SQL Script...");
        	this.startActivity(Intent.createChooser(emailIntent,"Sending email of Sensors & Locations data..."));
        	break;

			case R.id.menu_showvideopreview:
				if (mBound){
					mService.showWindow();
				}
				break;
			case R.id.menu_hidevideopreview:
				if (mBound){
					mService.hideWindow();
				}

				break;
			case R.id.menu_location_interval:
				showLocationIntervalSpinner();

				break;

        	// start action
        case R.id.action_start:
        	//Log.i(tag, "Start action with counter = " + Integer.toString(tripCounter));
        	SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
			String imei = sharedPreferences.getString(MapMyTripApp.wifiKey, "0");
			if (imei == "0" || !imei.contains(":")) {
	        	MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_INFO, "About to Register New Device while service starting...");
	        	new VehicleRegistrationTask(this).execute();
	        } 
        	this.isMapRecordingStarted = true;
        	
        	menuState = 1;
        	this.invalidateOptionsMenu();
        	
        	mMap.clear();
        	currentLocation = mMap.getMyLocation();
        	doNewTripDialog();
        	
        	return true;
	    }
        
        return true;
    }
    
    private void doNewTripDialog() {
    	AlertDialog.Builder ad = new AlertDialog.Builder(MapMyTripApp.this);
    	ad.setTitle("MapMyTrip");
    	ad.setMessage("Are you sure that you want to start new trip?");
    	ad.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		    	public void onClick(DialogInterface dialog, int whichButton) {
		    		positionReceiver = new PositionReceiver();
		    	    positionIntentFilter = new IntentFilter();
		    	    positionIntentFilter.addAction(MapTripUtility.PositionCoordinates);
		    	    registerReceiver(positionReceiver, positionIntentFilter);
		    	    
		    	    startNewTrip();
					startGPSLoggerService();
					startVideoRecordingService();
					if (currentLocation != null) {
		    			// addMarker(currentLocation, true);
		    			addMarker(currentLocation.getLatitude(), currentLocation.getLongitude(), 
		    					MapTripUtility.timestampFormat.format(currentLocation.getTime()),
		    					MapTripUtility.routeStartFlag);
		    			if (mMap != null) {
		    				LatLng currentlLatLon = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
		    				mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentlLatLon, 15));
		    			}

		    		}
		    	}
    		});
    	ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
    			public void onClick(DialogInterface dialog, int whichButton) {
    			}
    		});
    	
    	ad.show();
    }

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			moveTaskToBack(true);
		}
		return false;
	}
    
    private void startNewTrip() {
		try {
			tripCounter++;
			
			if(db != null) {
				long time = Calendar.getInstance().getTimeInMillis();
				this.currentTripName = db.insertRouteRecord(Long.toString(time));
				
				// Debug call
				Cursor c = db.getRouteCount();
				//Log.i(tag, "total record:" + c.getCount() + "tripCounter=" + tripCounter);
				//debug end
//				GregorianCalendar greg = new GregorianCalendar();
//				TimeZone tz = greg.getTimeZone();
//				int offset = tz.getOffset(System.currentTimeMillis());
//				greg.add(Calendar.SECOND, (offset/1000) * -1);
//				Time now = new Time();
//				now.setToNow();
				TimeZone tz = TimeZone.getTimeZone("GMT+03:00");
				Calendar cal = Calendar.getInstance(tz);
				if(currentLocation != null) {
					// TH! insert one record item
					PointRecord record = new PointRecord();
					record.setId(tripCounter);
					SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
					String deviceuniqueId = sharedPreferences.getString(MapMyTripApp.wifiKey, "0");
					record.setUid(deviceuniqueId);
					record.setTimeStamp(String.valueOf(MapTripUtility.timestampFormat.format(cal.getTimeInMillis())));
					record.setLatitude(currentLocation.getLatitude());
					record.setLongitude(currentLocation.getLongitude());
					
					if(currentLocation.hasAltitude())
						record.setAltitude(currentLocation.getAltitude());
					else
						record.setAltitude(0.0);
					
					if(currentLocation.hasAccuracy())
						record.setAccuracy((double)currentLocation.getAccuracy());
					else
						record.setAccuracy(0.0);
					
					if(currentLocation.hasSpeed())
						record.setSpeed((double)currentLocation.getSpeed());
					else
						record.setSpeed(0.0);
					
					if(currentLocation.hasBearing())
						record.setBearing((double)currentLocation.getBearing());
					else
						record.setBearing(0.0);
					
					db.insertPointRecord(tripCounter, record, MapTripDatabase.RECORD_UNREAD);
				}
			}
			
    	} catch (Exception e) {
    		//Log.e(tag, e.toString());
    		MapMyTripApp.logToFile(getApplicationContext(), MapMyTripApp.LOG_TAG_ERROR, "Exception while starting new trip:"+e.getMessage());
    	}
    }
    
    public MapTripDatabase getDB() {
		if (db == null) {
			MapTripDatabase.init(MapMyTripApp.this);
			db = MapTripDatabase.getInstance();
		}
		return db;
	}
    
    /**
     *
     * @param lat - latitude of the location to move the camera to
     * @param lng - longitude of the location to move the camera to
     *            Prepares a CameraUpdate object to be used with  callbacks
     */
    private void gotoMyLocation(double lat, double lng) {
        changeCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(new LatLng(lat, lng))
						.zoom(15.5f)
						.bearing(0)
						.tilt(25)
						.build()
		), new GoogleMap.CancelableCallback() {
			@Override
			public void onFinish() {
				// Your code here to do something after the Map is rendered
			}

			@Override
			public void onCancel() {
				// Your code here to do something after the Map rendering is cancelled
			}
		});
    }
    
    private void changeCamera(CameraUpdate update, GoogleMap.CancelableCallback callback) {
        mMap.moveCamera(update);
    }

    /**
     * Button to get current Location. This demonstrates how to get the current Location as required
     * without needing to register a LocationListener.
     */
    public void showMyLocation(View view) {
        if (mLocationClient != null && mLocationClient.isConnected()) {
            String msg = "Location = " + mLocationClient.getLastLocation();
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        }
    }
	
	private void addMarker(Double latitude, Double longitude, String time, int pointFlag) {
		try {
		DecimalFormat decimal = new DecimalFormat("#.######");
		SimpleDateFormat formatter = new SimpleDateFormat();
		String timeStamp = formatter.format(Long.parseLong(time));
		
		if(MapTripUtility.routeStartFlag == pointFlag) {
			mMap.addMarker(new MarkerOptions()
				.position(new LatLng(latitude, longitude))
				.title(decimal.format(latitude) + "," + decimal.format(longitude) + "," + timeStamp)
				.snippet("Trip start point")
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.start_marker)));	
		}
		else if(MapTripUtility.routeEndFlag == pointFlag){
			mMap.addMarker(new MarkerOptions()
				.position(new LatLng(latitude, longitude))
				.title(decimal.format(latitude) + "," + decimal.format(longitude) + "," + timeStamp)
				.snippet("Trip stop point")
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.stop_marker)));
		}
		else if(MapTripUtility.routePointFlag == pointFlag) {
			mMap.addMarker(new MarkerOptions()
        	.position(new LatLng(latitude, longitude))
        	.title(decimal.format(latitude) + "," + decimal.format(longitude) + "," + timeStamp)
        	.snippet("Trip marker")
        	.icon(BitmapDescriptorFactory.fromResource(R.drawable.dot)));
		}
		}
		catch (Exception e) {
			MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_ERROR, "Error while placing marker:"+e.getMessage());
		}
    }

	public class PositionReceiver extends BroadcastReceiver{
	 
		@Override
		public void onReceive(Context arg0, Intent arg1) {
			double latitude = arg1.getDoubleExtra("Latitude", 0.0);
			double longitude = arg1.getDoubleExtra("Longitude", 0.0);
			String timeStamp = arg1.getStringExtra("TimeStamp");
  
			//Log.i("MapMyTrip", "PositionReceiver = " + Double.toString(latitude) + " , " + Double.toString(longitude));
			
			addMarker(latitude, 
					longitude,
					timeStamp,
					MapTripUtility.routePointFlag);
			
			/*mMap.addMarker(new MarkerOptions()
			.position(new LatLng(latitude, longitude))
			.title("marker")
			.snippet("MapHajj start/stop ")
			.icon(BitmapDescriptorFactory.fromResource(R.drawable.dot)));*/
			if (currentLocation != null) {
				currentLocation.setLatitude(latitude);
				currentLocation.setLongitude(longitude);
				if (mMap != null) {
					LatLng currentlLatLon = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
					mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentlLatLon, 15));
				}

			}
		}
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub
		//Log.i(tag, "itemPosition:" + Integer.toString(itemPosition) + ", itemID: " + Long.toString(itemId));
		switch(itemPosition){
		case 0:
		case 1:
			
			//Log.i(tag, "*************** case 0 *************************");
			break;
		case 2:
			Intent web = new Intent(MapMyTripApp.this, WebActivity.class);
    		startActivity(web);
			break;
			
		case 3:
			Intent history = new Intent(MapMyTripApp.this, HistoryActivity.class);
    		startActivity(history);
			break;
		case 4:
			Intent intent = new Intent(MapMyTripApp.this, InfoActivity.class);
    		startActivity(intent);
			break;
		
		default:
			return false;
		}
		
		return false;
	}

	@Override
	public void onConnected(Bundle arg0) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		String imei = sharedPreferences.getString(MapMyTripApp.wifiKey, "0");
		if (imei == "0" || !imei.contains(":")) {
        	MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_INFO, "About to Register New Device while connected...");
        	new VehicleRegistrationTask(this).execute();
        } 
	}

	@Override
	public void onConnectionSuspended(int i) {

	}

	public void onDisconnected() {
		MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_INFO, "Internet is disconnected...");
    	
	}

	@Override
	protected void onDestroy() {
		try {
			if (positionReceiver != null)
			unregisterReceiver(positionReceiver);

			if (networkChangeReceiver!=null)
				unregisterReceiver(networkChangeReceiver);

			if (mBoundGPSLogger){
				mServiceGPSLogger.onDestroy();
			}

			stopGPSLoggerService();
			stopVideoRecordingService();
		}
		catch(Exception e) {
			MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_ERROR, "Exception while destroying:" + e.getMessage());
		}
		super.onDestroy();
	}

	/**
	 * Writes a message to the log file on the device.
	 * 
	 * @param logMessageTag
	 *            A tag identifying a group of log messages.
	 * @param logMessage
	 *            The message to add to the log.
	 */
	static void logToFile(Context context, String logMessageTag,
			String logMessage) {
		try {
			// Gets the log file from the root of the primary storage. If it
			// does
			// not exist, the file is created.
			SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
			String deviceUniqueId = sharedPreferences.getString(MapMyTripApp.wifiKey, "0");
			
			File rootFolder = new File(Environment.getExternalStorageDirectory(), "MapMyTrip");
			if (!rootFolder.exists()) 
				rootFolder.mkdirs();
			File logFile = new File(Environment.getExternalStorageDirectory(), "MapMyTrip/MapMyTrip_"+deviceUniqueId.replaceAll(":", "")+".txt");
			if (!logFile.exists()) {
				logFile.createNewFile();
			}
			// Write the message to the log with a timestamp
			BufferedWriter writer = new BufferedWriter(new FileWriter(logFile,
					true));
			writer.write(String.format("%1s [%2s]:%3s\r\n", getDateTimeStamp(),
					logMessageTag, logMessage));
			writer.close();
			// Refresh the data so it can seen when the device is plugged in a
			// computer. You may have to unplug and replug to see the latest
			// changes
			MediaScannerConnection.scanFile(context,
					new String[] { logFile.toString() }, null, null);

		} catch (IOException e) {
			Log.e("MapMyTrip Logger", "Unable to log exception to file.");
		}
	}
	
	/**
	 * Writes a message to the log file on the device.
	 * 
	 * @param sqlStatement
	 *            A tag identifying a group of log messages.
	 * @param logMessage
	 *            The message to add to the log.
	 */
	static void sqlPositionToFile(Context context, String sqlStatement) {
		try {
			// Gets the log file from the root of the primary storage. If it
			// does
			// not exist, the file is created.
			SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
			String deviceUniqueId = sharedPreferences.getString(MapMyTripApp.wifiKey, "0");
			
			File rootFolder = new File(Environment.getExternalStorageDirectory(), "MapMyTrip");
			if (!rootFolder.exists()) 
				rootFolder.mkdirs();
			File sqlFile = new File(Environment.getExternalStorageDirectory(), "MapMyTrip/MapMyTrip_"+deviceUniqueId.replaceAll(":", "")+".sql");
			if (!sqlFile.exists()) {
				sqlFile.createNewFile();
			}
			// Write the message to the log with a timestamp
			BufferedWriter writer = new BufferedWriter(new FileWriter(sqlFile, true));
			writer.write(String.format("%1s \r\n", sqlStatement));
			writer.close();
			// Refresh the data so it can seen when the device is plugged in a
			// computer. You may have to unplug and replug to see the latest
			// changes
			MediaScannerConnection.scanFile(context, new String[] { sqlFile.toString() }, null, null);

		} catch (IOException e) {
			Log.e("MapMyTrip Logger", "Unable to log exception to file.");
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_ERROR, "Exception while generating location sql:" + e.getMessage());
		}
	}
	static void sqlSensorToFile(Context context, String sqlStatement) {
		try {
			// Gets the log file from the root of the primary storage. If it
			// does
			// not exist, the file is created.
			SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
			String deviceUniqueId = sharedPreferences.getString(MapMyTripApp.wifiKey, "0");
			File rootFolder = new File(Environment.getExternalStorageDirectory(), "MapMyTrip");
			if (!rootFolder.exists()) 
				rootFolder.mkdirs();
			File sqlFile = new File(Environment.getExternalStorageDirectory(), "MapMyTrip/MapMyTripSensor_"+deviceUniqueId.replaceAll(":", "")+".sql");
			if (!sqlFile.exists()) 
				sqlFile.createNewFile();

			// Write the message to the log with a timestamp
			BufferedWriter writer = new BufferedWriter(new FileWriter(sqlFile, true));
			writer.write(String.format("%1s \r\n", sqlStatement));
			writer.close();
			// Refresh the data so it can seen when the device is plugged in a
			// computer. You may have to unplug and replug to see the latest
			// changes
			MediaScannerConnection.scanFile(context, new String[] { sqlFile.toString() }, null, null);

		} catch (IOException e) {
			Log.e("MapMyTrip Logger", "Unable to log exception to file.");
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_ERROR, "Exception while generating sensors sql:"+e.getMessage());
		}
	}
	/* Checks if external storage is available for read and write */
	public static boolean isExternalStorageWritable() {
	    String state = Environment.getExternalStorageState();
	    return Environment.MEDIA_MOUNTED.equals(state);
	}

	/* Checks if external storage is available to at least read */
	public static boolean isExternalStorageReadable() {
	    String state = Environment.getExternalStorageState();
	    return Environment.MEDIA_MOUNTED.equals(state) ||
	        Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
	}
	public static File getPublicStorageDir(String directory) {
	    // Get the directory for the user's public pictures directory. 
	    File file = new File(Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_PICTURES), directory);
	    if (!file.mkdirs()) {
	        Log.e(LOG_TAG_ERROR, "Directory not created");
	    }
	    return file;
	}
	public static File getPrivateStorageDir(Context context, String directory) {
	    // Get the directory for the app's private pictures directory. 
	    File file = new File(context.getExternalFilesDir(
				Environment.DIRECTORY_PICTURES), directory);
	    if (!file.mkdirs()) {
	        Log.e(LOG_TAG_ERROR, "Directory not created");
	    }
	    return file;
	}
	
	/**
	 * Gets a stamp containing the current date and time to write to the log.
	 * 
	 * @return The stamp for the current date and time.
	 */
	private static String getDateTimeStamp() {
		Date dateNow = Calendar.getInstance().getTime();
		// My locale, so all the log files have the same date and time format
		return (DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, Locale.CANADA_FRENCH).format(dateNow));
	}
	private void startVideoRecordingService(){

		bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
	}

	private void startGPSLoggerService(){

		bindService(intentGPSLoggerService, mConnectionGPSLogger, Context.BIND_AUTO_CREATE);
	}

	private void stopVideoRecordingService(){
		//Intent in=new Intent(MapMyTripApp.this,BackgroundVideoRecorder.class);
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}
		stopService(new Intent(intent));

	}
	private void stopGPSLoggerService(){
		//Intent in=new Intent(MapMyTripApp.this,GPSLoggerService.class);
		if (mBoundGPSLogger) {
			if (mServiceGPSLogger.candelLocalDataCollectionTask()){
				Toast.makeText(this, "Data Collection Task Canceled",
						Toast.LENGTH_SHORT).show();
			}
			if (mServiceGPSLogger.candelPostDataTimerTask()){
				Toast.makeText(this, "Data Post Task Canceled",
						Toast.LENGTH_SHORT).show();
			}

			unbindService(mConnectionGPSLogger);
			mBoundGPSLogger = false;
		}
		stopService(new Intent(intentGPSLoggerService));

	}

	/*
	private void startGPSLoggerService() {
		Bundle b=new Bundle();
		b.putLong("driverId", driverId);

		Intent in=new Intent(MapMyTripApp.this, GPSLoggerService.class);
		in.putExtras(b);
		startService(in);
	}

	*/
	private void showLocationIntervalSpinner(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		// Get the layout inflater
		LayoutInflater inflater = (this).getLayoutInflater();
		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the
		// dialog layout
		builder.setTitle("Select Interval (Sec)");
		builder.setCancelable(false);
		builder.setIcon(R.drawable.ic_launcher);
		View v=inflater.inflate(R.layout.location_data_interval_spinner, null);
		builder.setView(v)
				// Add action buttons
				.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				})
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {

				if (mBoundGPSLogger) {

					mServiceGPSLogger.candelLocalDataCollectionTask();

					mServiceGPSLogger.setupTimer(interval * 1000);
				}

			}
		});

		// Initialize Spinners

		final Spinner spIntervals = (Spinner) v.findViewById(R.id.spIntervals);
		ArrayAdapter arrayAdapter=new MyCustomAdapter(MapMyTripApp.this, R.layout.row, intervals);
		spIntervals.setAdapter(arrayAdapter);
		spIntervals.setSelection(arrayAdapter.getPosition(String.valueOf(interval)));
		spIntervals.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> adapter, View v,
									   int position, long id) {
				// On selecting a spinner item

				String item = adapter.getItemAtPosition(position).toString();

				// Showing selected spinner item
				//Toast.makeText(getApplicationContext(),
						//"Selected Interval : " + item, Toast.LENGTH_LONG).show();
				try {
					interval = Long.parseLong(item);
				} catch (Exception e) {
					e.printStackTrace();
					interval = 5;
					Toast.makeText(getApplicationContext(), "[Warn]Failed to set selected interval,defauld interval will be used. ", Toast.LENGTH_LONG).show();
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});


		builder.create();
		builder.show();
	}
	public class MyCustomAdapter extends ArrayAdapter<String>{

		public MyCustomAdapter(Context context, int textViewResourceId,
							   String[] objects) {
			super(context, textViewResourceId, objects);
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getDropDownView(int position, View convertView,
									ViewGroup parent) {
			// TODO Auto-generated method stub
			return getCustomView(position, convertView, parent);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			return getCustomView(position, convertView, parent);
		}

		public View getCustomView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			//return super.getView(position, convertView, parent);

			LayoutInflater inflater=getLayoutInflater();
			View row=inflater.inflate(R.layout.row, parent, false);
			TextView label=(TextView)row.findViewById(R.id.txtInterval);
			label.setText(intervals[position]);

			ImageView icon=(ImageView)row.findViewById(R.id.icon);

			if (intervals[position].equalsIgnoreCase(String.valueOf(interval))){
				icon.setImageResource(R.drawable.ic_action_play);
			}
			else{
				//icon.setImageResource(R.drawable.ic_action_stop);
			}

			return row;
		}
	}

}
