package com.uqu.mapmytrip;

import java.text.SimpleDateFormat;

public class MapTripUtility {

	public static final String baseURL="http://54.172.33.121:8080/";
	//public static final String baseURL="http://182.180.87.221:8080/Masar/";

	public static final String pointsTableName = "LOCATION_POINTS";
	
	public final static String PositionCoordinates = "POSITION_COORDS";
	
	public final static SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyyMMddHHmmss");

	// Map Marker flags
	public static final int routeStartFlag = 0;	//"ROUTE_START_POINT_FLAG";
	public static final int routeEndFlag = 	1;	//"ROUTE_END_POINT_FLAG";
	public static final int routePointFlag = 2;	//"ROUTE_POINT_FLAG";
	
	public static final String recordIdKey = "RECORD_ID_KEY";
	public static int routeId = -1;
	
	public static final String directoryName = "maphajjrecords"; 
	public static final String fileExtension = ".kml";
	
	public static String getRecordIdKey() {
		return recordIdKey;
	}
	
	public static int getRouteId() {
		return routeId;
	}
	
	public static void setRouteId(int id) {
		routeId = id;
	}
}
