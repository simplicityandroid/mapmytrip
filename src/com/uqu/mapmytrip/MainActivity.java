/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.uqu.mapmytrip;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.simplicity.io.mapmytrip.R;
import com.uqu.login.Driver;
import com.uqu.wizard.model.AbstractWizardModel;
import com.uqu.wizard.model.ModelCallbacks;
import com.uqu.wizard.model.Page;
import com.uqu.wizard.model.ReviewItem;
import com.uqu.wizard.ui.PageFragmentCallbacks;
import com.uqu.wizard.ui.ReviewFragment;
import com.uqu.wizard.ui.StepPagerStrip;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;



public class MainActivity extends FragmentActivity implements
        PageFragmentCallbacks,
        ReviewFragment.Callbacks,
        ModelCallbacks {
    private ViewPager mPager;
    private MyPagerAdapter mPagerAdapter;

    private boolean mEditingAfterReview;

    private AbstractWizardModel mWizardModel;

    private boolean mConsumePageSelectedEvent;

    private Button mNextButton;
    private Button mPrevButton;

    private List<Page> mCurrentPageSequence;
    private StepPagerStrip mStepPagerStrip;
    private static Context s_instance;
    ProgressDialog prgDialog;
    Driver driver;

    public static Context getContext() {
    	return s_instance;
    }
    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        s_instance = getApplicationContext();
        setContentView(R.layout.activity_main);

        prgDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);

        mWizardModel = new SandwichWizardModel(this);
        if (savedInstanceState != null) {
            mWizardModel.load(savedInstanceState.getBundle("model"));
        }

        mWizardModel.registerListener(this);

        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);
        mStepPagerStrip = (StepPagerStrip) findViewById(R.id.strip);
        mStepPagerStrip.setOnPageSelectedListener(new StepPagerStrip.OnPageSelectedListener() {
            @Override
            public void onPageStripSelected(int position) {
                position = Math.min(mPagerAdapter.getCount() - 1, position);
                if (mPager.getCurrentItem() != position) {
                    mPager.setCurrentItem(position);
                }
            }
        });

        mNextButton = (Button) findViewById(R.id.next_button);
        mPrevButton = (Button) findViewById(R.id.prev_button);

        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mStepPagerStrip.setCurrentPage(position);

                if (mConsumePageSelectedEvent) {
                    mConsumePageSelectedEvent = false;
                    return;
                }

                mEditingAfterReview = false;
                updateBottomBar();
            }
        });

        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPager.getCurrentItem() == mCurrentPageSequence.size()) {
                    DialogFragment dg = new DialogFragment() {
                        @Override
                        public Dialog onCreateDialog(Bundle savedInstanceState) {
                            return new AlertDialog.Builder(getActivity())
                                    .setMessage(R.string.submit_confirm_message)
                                    .setPositiveButton(R.string.submit_confirm_button, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {

                                            new HttpAsyncTaskCheckDriver().execute(MapTripUtility.baseURL+"get_driver.htm");


                                        }
                                    })
                                    .setNegativeButton(android.R.string.cancel, null)
                                    .create();
                        }
                    };
                    dg.show(getSupportFragmentManager(), "place_order_dialog");
                } else {
                    if (mEditingAfterReview) {
                        mPager.setCurrentItem(mPagerAdapter.getCount() - 1);
                    } else {
                        mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                    }
                }
            }
        });

        mPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
            }
        });

        onPageTreeChanged();
        updateBottomBar();
    }

    @Override
    public void onPageTreeChanged() {
        mCurrentPageSequence = mWizardModel.getCurrentPageSequence();
        recalculateCutOffPage();
        mStepPagerStrip.setPageCount(mCurrentPageSequence.size() + 1); // + 1 = review step
        mPagerAdapter.notifyDataSetChanged();
        updateBottomBar();
    }

    private void updateBottomBar() {
        int position = mPager.getCurrentItem();
        if (position == mCurrentPageSequence.size()) {
            mNextButton.setText(R.string.finish);
            //mNextButton.setBackgroundResource(R.drawable.finish_background);
            //mNextButton.setTextAppearance(this, R.style.TextAppearanceFinish);
        } else {
            mNextButton.setText(mEditingAfterReview
                    ? R.string.review
                    : R.string.next);
            mNextButton.setBackgroundResource(R.drawable.button_selector);
            TypedValue v = new TypedValue();
            getTheme().resolveAttribute(android.R.attr.textAppearanceMedium, v, true);
            //mNextButton.setTextAppearance(this, v.resourceId);
            mNextButton.setEnabled(position != mPagerAdapter.getCutOffPage());
        }

        mPrevButton.setVisibility(position <= 0 ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWizardModel.unregisterListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle("model", mWizardModel.save());
    }

    @Override
    public AbstractWizardModel onGetModel() {
        return mWizardModel;
    }

    @Override
    public void onEditScreenAfterReview(String key) {
        for (int i = mCurrentPageSequence.size() - 1; i >= 0; i--) {
            if (mCurrentPageSequence.get(i).getKey().equals(key)) {
                mConsumePageSelectedEvent = true;
                mEditingAfterReview = true;
                mPager.setCurrentItem(i);
                updateBottomBar();
                break;
            }
        }
    }

    @Override
    public void onPageDataChanged(Page page) {
        if (page.isRequired()) {
            if (recalculateCutOffPage()) {
                mPagerAdapter.notifyDataSetChanged();
                updateBottomBar();
            }
        }
    }

    @Override
    public Page onGetPage(String key) {
        return mWizardModel.findByKey(key);
    }

    private boolean recalculateCutOffPage() {
        // Cut off the pager adapter at first required page that isn't completed
        int cutOffPage = mCurrentPageSequence.size() + 1;
        for (int i = 0; i < mCurrentPageSequence.size(); i++) {
            Page page = mCurrentPageSequence.get(i);
            if (page.isRequired() && !page.isCompleted()) {
                cutOffPage = i;
                break;
            }
        }

        if (mPagerAdapter.getCutOffPage() != cutOffPage) {
            mPagerAdapter.setCutOffPage(cutOffPage);
            return true;
        }

        return false;
    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter {
        private int mCutOffPage;
        private Fragment mPrimaryItem;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            if (i >= mCurrentPageSequence.size()) {
                return new ReviewFragment();
            }

            return mCurrentPageSequence.get(i).createFragment();
        }

        @Override
        public int getItemPosition(Object object) {
            // TODO: be smarter about this
            if (object == mPrimaryItem) {
                // Re-use the current fragment (its position never changes)
                return POSITION_UNCHANGED;
            }

            return POSITION_NONE;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            mPrimaryItem = (Fragment) object;
        }

        @Override
        public int getCount() {
            if (mCurrentPageSequence == null) {
                return 0;
            }
            return Math.min(mCutOffPage + 1, mCurrentPageSequence.size() + 1);
        }

        public void setCutOffPage(int cutOffPage) {
            if (cutOffPage < 0) {
                cutOffPage = Integer.MAX_VALUE;
            }
            mCutOffPage = cutOffPage;
        }

        public int getCutOffPage() {
            return mCutOffPage;
        }
    }
    private  ArrayList<ReviewItem> getReviewItemsList(){
        ArrayList<ReviewItem> allReviewItems=new ArrayList<ReviewItem>();

        for (Page page:mCurrentPageSequence){
            ArrayList<ReviewItem> tmpReviewItems=new ArrayList<ReviewItem>();
            page.getReviewItems(tmpReviewItems);
            for (ReviewItem tmpR:tmpReviewItems){
                allReviewItems.add(tmpR);
            }
        }
        return allReviewItems;
    }
    public  String POST(String url, Driver driver){
        InputStream inputStream = null;
        String result = "";
        try {

            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost(url);

            String json = "";

            // 3. build jsonObject
            Gson gson =new Gson();
           json=gson.toJson(driver);
            // 5. set json to StringEntity
            StringEntity se = new StringEntity(json);

            // 6. set httpPost Entity
            httpPost.setEntity(se);

            // 7. Set some headers to inform server about the type of the content
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);


            int statusCode = httpResponse.getStatusLine().getStatusCode();

            Log.d("status code", "" + statusCode);

            if (statusCode!=200) {

                if (statusCode == 404) {
                    return "" + statusCode;
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    return "" + statusCode;
                }
                // When Http response code other than 404, 500
                else {

                    return ""+2000;
                }
            }


            // 9. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // 10. convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
            result=e.getLocalizedMessage();
        }

        // 11. return result
        return result;
    }
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
    private class HttpAsyncTaskAddDriver extends AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            prgDialog.show();
        }
        @Override
        protected String doInBackground(String... urls) {

            driver=new Driver();
            setDriverObject(driver);
            return POST(urls[0],driver);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            if (result!=null && result.contains(driver.getDriverName())){
                Gson gson =new Gson();
                Driver tmpDriver=gson.fromJson(result,Driver.class);
                finish();
                Toast.makeText(getApplicationContext(), "Successfully Registered", Toast.LENGTH_LONG).show();
                gotoMapMyTripApp(tmpDriver.getId());
            }else{
                if (result.contains("404"))
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                else
                if (result.contains("505"))
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                else
                if(result.contains("2000") || result.contains("refused"))
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(getApplicationContext(), "Failed to Register", Toast.LENGTH_LONG).show();
            }
        }

    }
    private class HttpAsyncTaskCheckDriver extends AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            prgDialog.show();
        }
        @Override
        protected String doInBackground(String... urls) {

            driver=new Driver();
            setDriverObject(driver);
            return POST(urls[0],driver);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            if (result!=null && result.contains(driver.getDriverId())){
                Gson gson = new Gson();
                Driver tmpDriver=gson.fromJson(result,Driver.class);
                if (tmpDriver.getId()>0){
                    Toast.makeText(getApplicationContext(), "Login already exists. Please try another id.", Toast.LENGTH_LONG).show();
                }else{
                    new HttpAsyncTaskAddDriver().execute(MapTripUtility.baseURL+"add_driver.htm");
                }
            }else{
                if (result.contains("404"))
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                else
                if (result.contains("505"))
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                else
                if(result.contains("2000") || result.contains("refused"))
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(getApplicationContext(), "Failed to check availability of login id.", Toast.LENGTH_LONG).show();
            }
        }

    }
    void gotoMapMyTripApp(long id) {
        Intent intent = new Intent(MainActivity.this, MapMyTripApp.class);
        intent.putExtra("driverId", id);
        //intent.putParcelableArrayListExtra("reviewItemList", getReviewItemsList());
        MainActivity.this.startActivity(intent);
    }

    void setDriverObject(Driver driver){
        for (ReviewItem rItem:getReviewItemsList()){
            if (rItem.getTitle().toLowerCase().contains("login"))
                driver.setDriverId(rItem.getDisplayValue());
            if (rItem.getTitle().toLowerCase().contains("name"))
                driver.setDriverName(rItem.getDisplayValue());
            if (rItem.getTitle().toLowerCase().contains("category"))
                driver.setDriverCategory(rItem.getDisplayValue());
            if (rItem.getTitle().toLowerCase().contains("gender"))
                driver.setGender(rItem.getDisplayValue());
            if (rItem.getTitle().toLowerCase().contains("education"))
                driver.setEducationLevel(rItem.getDisplayValue());
            if (rItem.getTitle().toLowerCase().contains("experience"))
                driver.setDriverExperience(rItem.getDisplayValue());
            if (rItem.getTitle().toLowerCase().contains("vehicle"))
                driver.setVehicleType(rItem.getDisplayValue());
            if (rItem.getTitle().toLowerCase().contains("glasses"))
                driver.setEyeGlasses(rItem.getDisplayValue());
            if (rItem.getTitle().toLowerCase().contains("comments"))
                driver.setRemarks(rItem.getDisplayValue());
            if (rItem.getTitle().toLowerCase().contains("age"))
                driver.setAgeRange(rItem.getDisplayValue());
            if (rItem.getTitle().toLowerCase().contains("password"))
                driver.setPassword(rItem.getDisplayValue());
            if(rItem.getTitle().toLowerCase().contains("nationality"))
                driver.setNationality(rItem.getDisplayValue());

        }

        WifiManager wifi = (WifiManager)getContext().getSystemService(Context.WIFI_SERVICE);
        String unique = null;
        if (wifi.getConnectionInfo() != null && wifi.getConnectionInfo().getMacAddress() != null)
            unique = wifi.getConnectionInfo().getMacAddress();
        else
            unique = android.os.Build.SERIAL+":1";

        driver.setDeviceId(unique);

    }
}
