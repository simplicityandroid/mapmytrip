/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.uqu.mapmytrip;


import android.content.Context;

import com.simplicity.io.mapmytrip.R;
import com.uqu.wizard.model.AbstractWizardModel;
import com.uqu.wizard.model.BranchPage;
import com.uqu.wizard.model.CustomerInfoPage;
import com.uqu.wizard.model.PageList;
import com.uqu.wizard.model.SingleFixedChoicePage;

public class SandwichWizardModel extends AbstractWizardModel {
        public SandwichWizardModel(Context context) {
                super(context);
        }

        @Override
        protected PageList onNewRootPageList() {
                return new PageList(
                        new CustomerInfoPage(this, "Driver Detail")
                                .setRequired(true),

                        new BranchPage(this, MainActivity.getContext().getString(R.string.driver_category))

                                .addBranch(MainActivity.getContext().getString(R.string.driver_type_personal),

                                        new SingleFixedChoicePage(this, MainActivity.getContext().getString(R.string.nationality))
                                                .setChoices(MainActivity.getContext().getString(R.string.saudi), MainActivity.getContext().getString(R.string.arab_gluf_states),MainActivity.getContext().getString(R.string.arab_states),MainActivity.getContext().getString(R.string.southeast_asia),MainActivity.getContext().getString(R.string.europe_and_america),MainActivity.getContext().getString(R.string.african))
                                                .setRequired(true),

                                        new SingleFixedChoicePage(this, MainActivity.getContext().getString(R.string.education_level))
                                                .setChoices(MainActivity.getContext().getString(R.string.no_schooling_completed), MainActivity.getContext().getString(R.string.high_school_graduate), MainActivity.getContext().getString(R.string.colloge_graduate), MainActivity.getContext().getString(R.string.bachelor_degree),MainActivity.getContext().getString(R.string.masters_degree),MainActivity.getContext().getString(R.string.doctorate_degree))
                                                .setRequired(true),

                                        new SingleFixedChoicePage(this, MainActivity.getContext().getString(R.string.driver_experience))
                                                .setChoices(MainActivity.getContext().getString(R.string.level_low), MainActivity.getContext().getString(R.string.level_medium), MainActivity.getContext().getString(R.string.level_high))
                                                .setRequired(true),

                                        new SingleFixedChoicePage(this, MainActivity.getContext().getString(R.string.vehicle_type))
                                                .setChoices(MainActivity.getContext().getString(R.string.ltv), MainActivity.getContext().getString(R.string.htv))
                                                .setRequired(true),

                                        new SingleFixedChoicePage(this, MainActivity.getContext().getString(R.string.wearing_glasses))
                                                .setChoices(MainActivity.getContext().getString(R.string.no), MainActivity.getContext().getString(R.string.yes))
                                                .setRequired(true),


                                        new SingleFixedChoicePage(this, MainActivity.getContext().getString(R.string.age_range))
                                                .setChoices(MainActivity.getContext().getString(R.string.less_20), MainActivity.getContext().getString(R.string.range_20_40), MainActivity.getContext().getString(R.string.range_40_60), MainActivity.getContext().getString(R.string.older_60))
                                                .setRequired(true)

                                )
                                .addBranch(MainActivity.getContext().getString(R.string.driver_type_professional),

                                        new SingleFixedChoicePage(this, MainActivity.getContext().getString(R.string.nationality))
                                                .setChoices(MainActivity.getContext().getString(R.string.saudi), MainActivity.getContext().getString(R.string.arab_gluf_states),MainActivity.getContext().getString(R.string.arab_states),MainActivity.getContext().getString(R.string.southeast_asia),MainActivity.getContext().getString(R.string.europe_and_america),MainActivity.getContext().getString(R.string.african))
                                                .setRequired(true),

                                        new SingleFixedChoicePage(this, MainActivity.getContext().getString(R.string.education_level))
                                                .setChoices(MainActivity.getContext().getString(R.string.no_schooling_completed), MainActivity.getContext().getString(R.string.high_school_graduate), MainActivity.getContext().getString(R.string.colloge_graduate), MainActivity.getContext().getString(R.string.bachelor_degree),MainActivity.getContext().getString(R.string.masters_degree),MainActivity.getContext().getString(R.string.doctorate_degree))
                                                .setRequired(true),

                                        new SingleFixedChoicePage(this, MainActivity.getContext().getString(R.string.driver_experience))
                                                .setChoices(MainActivity.getContext().getString(R.string.level_low), MainActivity.getContext().getString(R.string.level_medium), MainActivity.getContext().getString(R.string.level_high))
                                                .setRequired(true),

                                        new SingleFixedChoicePage(this, MainActivity.getContext().getString(R.string.vehicle_type))
                                                .setChoices(MainActivity.getContext().getString(R.string.ltv), MainActivity.getContext().getString(R.string.htv))
                                                .setRequired(true),

                                        new SingleFixedChoicePage(this, MainActivity.getContext().getString(R.string.wearing_glasses))
                                                .setChoices(MainActivity.getContext().getString(R.string.no), MainActivity.getContext().getString(R.string.yes))
                                                .setRequired(true),


                                        new SingleFixedChoicePage(this, MainActivity.getContext().getString(R.string.age_range))
                                                .setChoices(MainActivity.getContext().getString(R.string.less_20), MainActivity.getContext().getString(R.string.range_20_40), MainActivity.getContext().getString(R.string.range_40_60), MainActivity.getContext().getString(R.string.older_60))
                                                .setRequired(true)
                                ).setRequired(true)

                );
        }
}
