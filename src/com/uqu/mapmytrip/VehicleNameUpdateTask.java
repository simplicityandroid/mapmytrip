package com.uqu.mapmytrip;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

class VehicleNameUpdateTask extends AsyncTask<URL, Integer, Long> {
	private static final String URL_UPDATE_NAME = "http://54.172.33.121:8080/update_user.htm";
	private List<NameValuePair> nameValuePairs;
	private Context mContext;

	public VehicleNameUpdateTask(Context context) {
		mContext = context;
	}

	protected Long doInBackground(URL... urls) {
		long id = -1;

		// Creating service handler class instance
		ServiceHandler serviceHandler = new ServiceHandler();
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
		String deviceUniqueId = sharedPreferences.getString(MapMyTripApp.wifiKey, "0");

		String name = MapMyTripApp.deviceName;
		try {
			nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("uniqueid", deviceUniqueId));
			nameValuePairs.add(new BasicNameValuePair("name", name));
			String regStr = serviceHandler.makeServiceCall(URL_UPDATE_NAME, ServiceHandler.POST, nameValuePairs);
			if (regStr == null || regStr.equals(""))
				MapMyTripApp.logToFile(mContext, MapMyTripApp.LOG_TAG_ERROR, "Failed to update Device...");
			else {
				MapMyTripApp.logToFile(mContext, MapMyTripApp.LOG_TAG_INFO, "Device name updated:" + regStr);
				id = 1;
			}
		} catch (/* ClientProtocol */Exception e) {
			MapMyTripApp.logToFile(mContext, MapMyTripApp.LOG_TAG_ERROR, e.getMessage());
		}

		return id;
	}

	protected void onProgressUpdate(Integer... progress) {
	}

	protected void onPostExecute(Long result) {
	}
}
