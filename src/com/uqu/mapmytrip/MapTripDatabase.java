package com.uqu.mapmytrip;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MapTripDatabase extends SQLiteOpenHelper {
	
	public static final String tag = "MapHajjDatabase";
	private Semaphore writeSemaphore = new Semaphore(1);
	
	private static final String DB_NAME = "mapmyhajj.db";
	private static final int VERSION = 1;
	private static final String POINTS_TABLE_NAME = "data_tbl";
	private static final String RECORD_TABLE_NAME = "record_tbl";
	public static int RECORD_READ = 1;
	public static int RECORD_UNREAD = 0;
	
	@SuppressWarnings("javadoc")
	public static class RecordColumn {
		public static final String ID = "_id";
		public static final String TRIPNAME = "tripname";
		public static final String TIMESTAMP = "timestamp";
	}
	
	@SuppressWarnings("javadoc")
	public static class PointsColumn {
		public static final String ID = "_id";
		public static final String TIMESTAMP = "timestamp";
		public static final String LATITUDE = "latitude";
		public static final String LONGITUDE = "longitude";
		public static final String OPERATOR = "operator";
		public static final String STRENGTH = "strength";
		public static final String ALTITUDE = "altitude";
		public static final String ACCURACY= "accuracy";
		public static final String SPEED = "speed";
		public static final String BEARING = "bearing";
		public static final String UID = "uid";
		public static final String TRIPID = "tid";
		public static final String ISREAD = "isread";

		public static final String ACCX = "accx";
		public static final String ACCY = "accy";
		public static final String ACCZ = "accz";
		public static final String GYRX = "gyrx";
		public static final String GYRY = "gyry";
		public static final String GYRZ = "gyrz";
		public static final String MAGX = "magx";
		public static final String MAGY = "magy";
		public static final String MAGZ = "magz";
		public static final String LIGHT = "light";
		public static final String NETWORK = "network";
		public static final String DRIVERID = "driverid";

	}
	
	private static MapTripDatabase instance;

	private final Context context;

	public MapTripDatabase(Context context) {
		super(context, DB_NAME, null, VERSION);
		this.context = context;
	}
	
	/**
	 * Initialize database.
	 * 
	 * @param context
	 */
	public static void init(Context context) {
		if (MapTripDatabase.instance == null) {
			MapTripDatabase.instance = new MapTripDatabase(context);
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_INFO, "Database is initializing...");
		} else {
			Log.i(tag, "MapHajjDatabase is already initialized");
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_INFO, "MapMyTrip Database is already initialized...");
		}
	}
	
	/**
	 * Get singleton object of database.
	 * 
	 * @return singleton
	 */
	public static MapTripDatabase getInstance() {

		if (MapTripDatabase.instance == null) {
			return null;
		}
		
		return MapTripDatabase.instance;
	}


	@Override
	public void onCreate(SQLiteDatabase db) {
		createRecordTable(db);
		
		createPointsTable(db);
	}
	
	/**
	 * Create Record table
	 */
	private void createRecordTable(SQLiteDatabase db) {
		StringBuilder sql;
		
		// Categories
		sql = new StringBuilder();
		sql.append("CREATE TABLE IF NOT EXISTS ").append(RECORD_TABLE_NAME).append(" (");
		sql.append(RecordColumn.ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT,");
		sql.append(RecordColumn.TRIPNAME).append(" TEXT,");
		sql.append(RecordColumn.TIMESTAMP).append(" TEXT");
		sql.append(")");
		
		try {
			//Log.i(tag, "Record table created successfully." + sql.toString());
			db.execSQL(sql.toString());
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_INFO, "History Record table created successfully...");
		}catch(SQLiteException e) {
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_ERROR, "History Record table create Error...");
			Log.e(tag, "Failed to create record table");
			e.printStackTrace();
		}
	}

	
	/**
	 * Create Points table
	 */
	private void createPointsTable(SQLiteDatabase db) {
		StringBuilder sql;
		
		// Categories
		sql = new StringBuilder();
		sql.append("CREATE TABLE IF NOT EXISTS ").append(POINTS_TABLE_NAME).append(" (");
		sql.append(PointsColumn.ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT,");
		sql.append(PointsColumn.TIMESTAMP).append(" TEXT,");
		sql.append(PointsColumn.NETWORK).append(" TEXT,");
		sql.append(PointsColumn.LATITUDE).append(" DOUBLE,");
		sql.append(PointsColumn.LONGITUDE).append(" DOUBLE,");
		sql.append(PointsColumn.OPERATOR).append(" TEXT,");
		sql.append(PointsColumn.STRENGTH).append(" DOUBLE,");
		sql.append(PointsColumn.ALTITUDE).append(" DOUBLE,");
		sql.append(PointsColumn.ACCURACY).append(" DOUBLE,");
		sql.append(PointsColumn.SPEED).append(" DOUBLE,");
		sql.append(PointsColumn.BEARING).append(" DOUBLE,");
		sql.append(PointsColumn.UID).append(" TEXT,");
		sql.append(PointsColumn.TRIPID).append(" INTEGER,");
		sql.append(PointsColumn.ACCX).append(" INTEGER,");
		sql.append(PointsColumn.ACCY).append(" INTEGER,");
		sql.append(PointsColumn.ACCZ).append(" INTEGER,");
		sql.append(PointsColumn.GYRX).append(" INTEGER,");
		sql.append(PointsColumn.GYRY).append(" INTEGER,");
		sql.append(PointsColumn.GYRZ).append(" INTEGER,");
		sql.append(PointsColumn.MAGX).append(" INTEGER,");
		sql.append(PointsColumn.MAGY).append(" INTEGER,");
		sql.append(PointsColumn.MAGZ).append(" INTEGER,");
		sql.append(PointsColumn.LIGHT).append(" INTEGER,");
		sql.append(PointsColumn.ISREAD).append("  INTEGER,");
		sql.append(PointsColumn.DRIVERID).append("  INTEGER");
		sql.append(")");
		try {
			//Log.i(tag, "Points table created successfully." + sql.toString());
			db.execSQL(sql.toString());
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_INFO, "Location table created successfully...");
		}catch(SQLiteException e) {
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_ERROR, "Location table create error...");
			Log.e(tag, "Failed to create points table");
			e.printStackTrace();
		}
	}
	
	static int dummyCount = 1;
	
	/**
	 * @return
	 */
	public long getLastInsertId() {
		long index = 0;
		SQLiteDatabase sdb = getReadableDatabase();
		Cursor cursor = sdb.query("sqlite_sequence", new String[] { "seq" },
				"name = ?", new String[] { POINTS_TABLE_NAME }, null, null, null, null);
		if (cursor.moveToFirst()) {
			index = cursor.getLong(cursor.getColumnIndex("seq"));
		}
		cursor.close();
		return index;
	}

	/**
	 * Insert one particular map record.
	 * 
	 * @param Route
	 */
	public synchronized String insertRouteRecord(String timeStamp) {
		writeSemaphore.acquireUninterruptibly();
		SQLiteDatabase db = getWritableDatabase();
		
		String trip = "trip_";

		Cursor c = getRouteCount();
		
		if(c != null)
			trip = trip + (c.getCount() + 1);
		
		ContentValues values = new ContentValues();
		
		values.put(RecordColumn.TRIPNAME, trip);
		values.put(RecordColumn.TIMESTAMP, timeStamp);
		
		try {
			db.insertOrThrow(RECORD_TABLE_NAME, null, values);
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_INFO, "Trip information stored in local DB");
			//Log.i(tag, "Route Record inserted : "+ dummyCount + "," + trip);
		} catch (SQLException e) {
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_ERROR, "Error while Trip information storing in local DB");
			Log.e(tag, "database insert record fail: " + e);
		}
		
		writeSemaphore.release();
		
		return trip;
	}
	
	/**
	 * Update new/existing channels to database.
	 * 
	 * @param channels
	 */
	public synchronized void insertPointRecord(int id, PointRecord record, int isread) {
		// Backup Insert Statement on Device Storage...
		String sqlStatement = "INSERT INTO `mapmytrip`.`positions` (`device_id`, `latitude`, `longitude`, `altitude`, `speed`, `time`, `post_time`, `network_type`, `operator`, `strength`) VALUES ('"+
				record.getUid()+"','"+record.getLatitude()+"','"+record.getLongitude()+"','"+record.getAltitude()+"','"+record.getSpeed()+"',STR_TO_DATE('"+record.getTimeStamp()+"','%Y%m%d%H%i%s%p'),"+"STR_TO_DATE('"+record.getPostTimeStamp()+"','%Y%m%d%H%i%s%p'),'"+record.getNetworkType()+"','"+record.getOperator()+"','"+record.getSignalStrength()+"');";
		
//		String sqlStatement = "INSERT INTO `mapmytrip`.`positions` (`device_id`, `latitude`, `longitude`, `speed`, `time`, `altitude`, `accx`, `accy`, `accz`, `gyrx`, `gyry`,`gyrz`, `magx`, `magy`, `magz`, `light`) VALUES ('"+
//				record.getUid()+"','"+record.getLatitude()+"','"+record.getLongitude()+"','"+record.getSpeed()+"',STR_TO_DATE('"+record.getTimeStamp()+"','%d-%M-%y %h.%i.%s %p'),'" +record.getAltitude() +"','"+record.getAccX()+"','"+record.getAccY()+"','"+record.getAccZ()+"','"+record.getGyrX()+"','"+record.getGyrY()+"','"+record.getGyrZ()+"','"+record.getMagX()+"','"+record.getMagY()+"','"+record.getMagZ()+"','"+record.getLight()+"');";

		MapMyTripApp.sqlPositionToFile(context, sqlStatement);
		
		writeSemaphore.acquireUninterruptibly();
		SQLiteDatabase db = getWritableDatabase();

		// values
		ContentValues values = new ContentValues();
		
		values.put(PointsColumn.ID, getLastInsertId() + 1);
		values.put(PointsColumn.TIMESTAMP, record.getTimeStamp());
		values.put(PointsColumn.NETWORK , record.getNetworkType());
		values.put(PointsColumn.LATITUDE, record.getLatitude());
		values.put(PointsColumn.LONGITUDE, record.getLongitude() );
		values.put(PointsColumn.OPERATOR, record.getOperator() );
		values.put(PointsColumn.STRENGTH, record.getSignalStrength() );
		values.put(PointsColumn.ALTITUDE, record.getAltitude());
		values.put(PointsColumn.ACCURACY, record.getAccuracy());
		values.put(PointsColumn.SPEED, record.getSpeed());
		values.put(PointsColumn.BEARING, record.getBearing());
		values.put(PointsColumn.UID, record.getUid());
		values.put(PointsColumn.ACCX, record.getAccX());
		values.put(PointsColumn.ACCY, record.getAccY());
		values.put(PointsColumn.ACCZ, record.getAccZ());
		values.put(PointsColumn.GYRX, record.getGyrX());
		values.put(PointsColumn.GYRY, record.getGyrY());
		values.put(PointsColumn.GYRZ, record.getGyrZ());
		values.put(PointsColumn.MAGX, record.getMagX());
		values.put(PointsColumn.MAGY, record.getMagY());
		values.put(PointsColumn.MAGZ, record.getMagZ());
		values.put(PointsColumn.LIGHT, record.getLight());
		values.put(PointsColumn.NETWORK, record.getNetworkType());
		values.put(PointsColumn.TRIPID, id);
		values.put(PointsColumn.ISREAD, isread);
		values.put(PointsColumn.DRIVERID,record.getDriverId());

		try {
			db.insertOrThrow(POINTS_TABLE_NAME, null, values);
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_INFO, "Location information stored in local DB...");
			//Log.i(tag, "Point Record inserted : "+ id + "," + dummyCount);
		} catch (SQLException e) {
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_ERROR, "Error while Location information storing in local DB");
			Log.e(tag, "database insert record fail: " + e);
		}
		
		writeSemaphore.release();
	}
	
	// Clear Record
	public synchronized void clearRecordRequest(int id) {
		int status = -1;
		writeSemaphore.acquireUninterruptibly();

		SQLiteDatabase db = getWritableDatabase();
		
		String where = RecordColumn.ID + " = " + id;
		
		try {
			status = db.delete(RECORD_TABLE_NAME, where, null);
		
			where = PointsColumn.ID + " = " + id;
			status = db.delete(POINTS_TABLE_NAME, where, null);
		} catch(SQLiteException e) {
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_ERROR, "Exception while clear DB records:"+e.getMessage());
			Log.e(tag, "Cleared records with status:" +  status);
		}
		
		db.setTransactionSuccessful();
		db.endTransaction();

		writeSemaphore.release();
	}
	
	public synchronized Cursor getRoute(int id) {
		Cursor c = null;
		
		//Log.i(tag, "Get Route:: (id, dummyCount) "+ id + "," +dummyCount);
		
		String selection = RecordColumn.ID + " = " + id;
		String order = PointsColumn.TIMESTAMP;
		
		try {
			Cursor tempCursor = getReadableDatabase().query(RECORD_TABLE_NAME, 
													null, selection, null, null, null, null);
			
			if(tempCursor.getCount() > 0) {
				selection = PointsColumn.TRIPID + " = " + id;
				c = getReadableDatabase().query(POINTS_TABLE_NAME, null, selection, null, null, null, order);
				MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_INFO, "Trip information found from local DB...");
			}
		} catch(Exception e) {
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_ERROR, "Error while Trip information find from local DB...");
			e.printStackTrace();
		}
		
		return c;
	}
	
	
	public synchronized Cursor getRouteCount() {
		Cursor c = null;
		
		try {
			c = getReadableDatabase().rawQuery("SELECT * FROM " + RECORD_TABLE_NAME, null);
		} catch(Exception e) {
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_ERROR, "Exception while getting route counts from DB:"+e.getMessage());
			e.printStackTrace();
		}
		
		//Log.i(tag, "GetRouteCount:: total = " + c.getCount());
		
		if(c == null)
			return null;
		
		return c;
	}
	
	public synchronized int getRecordId(String recordTitle) {
		String where = RecordColumn.TRIPNAME + " = " + "'" + recordTitle + "'";
		Cursor cursor = null;
		
		try {
			cursor = getReadableDatabase().query(RECORD_TABLE_NAME, 
					null, where, null, null, null, null);
			
			if(cursor == null)
				return -1;
			else
				cursor.moveToFirst();
			
		} catch (Exception e) {
			MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_ERROR, "Exception while getting trip id from DB:"+e.getMessage());
			//Log.e(tag, "Failed to get record with tripTitle: " + recordTitle);
			return -1;
		}
		
		return cursor.getInt(0);
	}
	public PointRecord[] getPositionDetail() {

        String selectQuery = "SELECT  * FROM " + POINTS_TABLE_NAME;
              SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<PointRecord> dataz = new ArrayList<PointRecord>();
        if (cursor.moveToFirst()) {
            do {
				PointRecord record = new PointRecord();
				record.setId(cursor.getInt(0));
				record.setTimeStamp(cursor.getString(1));
				record.setLatitude(cursor.getDouble(2));
				record.setLongitude(cursor.getDouble(3));
				
				record.setAltitude(cursor.getDouble(4));
				
				record.setAccuracy(cursor.getDouble(5));
				record.setSpeed(cursor.getDouble(6));
				record.setBearing(cursor.getDouble(7));
				record.setDriverId(cursor.getInt(cursor.getColumnIndex("driverid")));
				dataz.add(record);

               // get  the  data into array,or class variable
            } while (cursor.moveToNext());
        }
        db.close();
        return dataz.toArray(new PointRecord[dataz.size()]);
    }
	
	public synchronized List<PointRecord> getUnreadPositionDetail() {

        String selectQuery = "SELECT  * FROM " + POINTS_TABLE_NAME+ " where ISREAD = 0";
              SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<PointRecord> dataz = new ArrayList<PointRecord>();
        if (cursor.moveToFirst()) {
            do {
				PointRecord record = new PointRecord();
				record.setId(cursor.getInt(cursor.getColumnIndex(PointsColumn.ID)));
				record.setTimeStamp(cursor.getString(cursor.getColumnIndex(PointsColumn.TIMESTAMP)));
				record.setLatitude(cursor.getDouble(cursor.getColumnIndex(PointsColumn.LATITUDE)));
				record.setLongitude(cursor.getDouble(cursor.getColumnIndex(PointsColumn.LONGITUDE)));
				record.setAltitude(cursor.getDouble(cursor.getColumnIndex(PointsColumn.ALTITUDE)));
				record.setAccuracy(cursor.getDouble(cursor.getColumnIndex(PointsColumn.ACCURACY)));
				record.setSpeed(cursor.getDouble(cursor.getColumnIndex(PointsColumn.SPEED)));
				record.setBearing(cursor.getDouble(cursor.getColumnIndex(PointsColumn.BEARING)));
				record.setDriverId(cursor.getInt(cursor.getColumnIndex(PointsColumn.DRIVERID)));
				record.setNetworkType(cursor.getString(cursor.getColumnIndex(PointsColumn.NETWORK)));
				record.setOperator(cursor.getString(cursor.getColumnIndex(PointsColumn.OPERATOR)));
				record.setUid(cursor.getString(cursor.getColumnIndex(PointsColumn.UID)));
				record.setOperatorName(cursor.getString(cursor.getColumnIndex(PointsColumn.OPERATOR)));
				record.setSignalStrength(cursor.getInt(cursor.getColumnIndex(PointsColumn.STRENGTH)));


				dataz.add(record);

               // get  the  data into array,or class variable
            } while (cursor.moveToNext());
        }
        db.close();
        return dataz;
    }

	// delete trip
	public synchronized void deleteTrip(int id) {
		
		writeSemaphore.acquireUninterruptibly();
		SQLiteDatabase db = getWritableDatabase();
		
		String where = RecordColumn.ID + " = " + id;
		
		try {
        	if(db != null) {
        		db.delete(RECORD_TABLE_NAME, where, null);
        		
        		where = PointsColumn.ID + " = " + id;
        		db.delete(POINTS_TABLE_NAME, where, null);
        	}
    	} catch (SQLiteException e) {
    		MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_ERROR, "Exception while deleting trip from DB:"+e.getMessage());
    		Log.e(tag, "Failed to delete record with id: " + id);
    	}

		writeSemaphore.release();
	}
	// delete trip
		public synchronized void deletePoint(int id) {
			
			writeSemaphore.acquireUninterruptibly();
			SQLiteDatabase db = getWritableDatabase();
			
			try {
	        	if(db != null) {
	        		String where = PointsColumn.ID + " = " + id;
	        		db.delete(POINTS_TABLE_NAME, where, null);
	        	}
	    	} catch (SQLiteException e) {
	    		MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_ERROR, "Exception while deleting location from DB:"+e.getMessage());
	    		Log.e(tag, "Failed to delete record with id: " + id);
	    	}

			writeSemaphore.release();
		}
		// set trip read
				public synchronized void setPointRead(int id) {
					
					writeSemaphore.acquireUninterruptibly();
					SQLiteDatabase db = getWritableDatabase();
					
					try {
			        	if(db != null) {
			        		String where = PointsColumn.ID + " = " + id;
			        		ContentValues cv = new ContentValues();
			        		cv.put("ISREAD",1); 
			        		db.update(POINTS_TABLE_NAME, cv, where, null);
			        	}
			    	} catch (SQLiteException e) {
			    		MapMyTripApp.logToFile(context, MapMyTripApp.LOG_TAG_ERROR, "Exception while setting location read from DB:"+e.getMessage());
			    		Log.e(tag, "Failed to delete record with id: " + id);
			    	}

					writeSemaphore.release();
				}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		if(oldVersion == 3) {
			Log.i(tag, "Database version: " + oldVersion + "," + newVersion);
		}	
	}
}
