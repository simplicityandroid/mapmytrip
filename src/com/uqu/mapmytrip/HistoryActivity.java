package com.uqu.mapmytrip;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.simplicity.io.mapmytrip.R;

public class HistoryActivity extends ListActivity implements
										LoaderManager.LoaderCallbacks<Cursor>
{
	private static final String tag = "HistoryActivity";
	private HistoryAdapter historyAdapter;
	private LoaderManager loaderManager;
	public final static String LoadMapRecordNotificatoion = "MAP_RECORD_NOTIFY";
	
	//private	int		tripId;
	private Cursor	cursor;
	protected MapTripDatabase db;
	protected static int loaderID = 0;

	protected Object mActionMode;
	public int selectedItem = -1;
	public int selectedId = -1;
	public String selectedItemTitle;
	private AlertDialog.Builder dialogBuilder;
	private final DecimalFormat sevenSigDigits = new DecimalFormat("0.#######");
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(tag, "KioskItemActivity::onCreate callled");
		MapMyTripApp.logToFile(this, MapMyTripApp.LOG_TAG_INFO, "onCreate called of History Activity...");
		
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_history_layout);
		
		getActionBar().setTitle("Map Records");
		getActionBar().setDisplayHomeAsUpEnabled(false);
		
		db = MapTripDatabase.getInstance();
		if (db == null) {
			MapTripDatabase.init(this);
			db = MapTripDatabase.getInstance();
		}
		
		//tripId = getIntent().getIntExtra("tirp_id", 0);
	
		cursor = db.getRouteCount();
		historyAdapter = new HistoryAdapter(this, cursor);
		
		setListAdapter(historyAdapter);
		
		if(cursor.getCount() <= 0 ) {
			Toast toast = Toast.makeText(getApplicationContext(),
	                "History Status: No History found", Toast.LENGTH_SHORT);
	        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
	        toast.show();
		}
		
		getListView().setOnItemLongClickListener(new OnItemLongClickListener() {

		      @Override
		      public boolean onItemLongClick(AdapterView<?> parent, View view,
		          int position, long id) {

		        if (mActionMode != null) {
		          return false;
		        }
		        selectedItem = position;
		        selectedId = (int)id;
		        
		        TextView txtTitle = (TextView) view.findViewById(R.id.tripname);
		        selectedItemTitle = (String) txtTitle.getText();

		        // start the CAB using the ActionMode.Callback defined above
		        mActionMode = HistoryActivity.this.startActionMode(mActionModeCallback);
		        
		        view.setSelected(true);
		        return true;
		      }
		    });
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		case android.R.id.home:
			getActionBar().setDisplayHomeAsUpEnabled(false);
	        return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		
		if(db == null)
			return null;
		
		cursor = db.getRouteCount();
		
		//Log.i(tag, "Item count:" + cursor.getCount());
		
		if(cursor.getCount() <= 0 ) {
			Toast toast = Toast.makeText(getApplicationContext(),
	                "No Items found in this category", Toast.LENGTH_SHORT);
	        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
	        toast.show();
		}
		
		historyAdapter = new HistoryAdapter(this, cursor);
		setListAdapter(historyAdapter);
		loaderManager  = getLoaderManager();
		loaderManager.initLoader(loaderID, null, this);
		
		return new CursorLoader(this);
	}
	
	// Called when the Loader has finished loading its data
	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		//Log.i(tag,"OnLoadFinished: " + data.getCount());
		if(historyAdapter!=null && cursor!=null)
			historyAdapter.swapCursor(cursor); //swap the new cursor in.
		else {
			historyAdapter.swapCursor(data);
			//Log.i(tag,"OnLoadFinished: itemAdapter is null");
		}
	}
		
	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		//Log.i(tag, "onLoaderReset called");
		if(historyAdapter!=null)
			historyAdapter.swapCursor(null);

	}
	
	
	@Override 
	public void onListItemClick(ListView l, View v, int position, long id) {
		//Log.i(tag, "onListItemClick:(pos,catid,id)" + position + "," + id);
		
		
		Toast toast = Toast.makeText(getApplicationContext(),
                "Trip " + Integer.toString(position+1) + " is going to display.", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
        
        TextView txtTitle = (TextView) v.findViewById(R.id.tripname);
        //Log.i(tag, "Item Title: " + txtTitle.getText());
        
        MapMyTripApp.tripCounter = (int)id;
        callHomeActivity(id);
	}
	
	public void callHomeActivity(long id) {
		MapTripUtility.setRouteId((int)id);
		onBackPressed();
	}
	
	private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

	    // called when the action mode is created; startActionMode() was called
	    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
	      // Inflate a menu resource providing context menu items
	      MenuInflater inflater = mode.getMenuInflater();
	      // assumes that you have "contexual.xml" menu resources
	      inflater.inflate(R.menu.maptrip_context_menu, menu);
	      return true;
	    }

	    // Called after onCreateActionMode each time.
	    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
	    	return false; // Return false if nothing is done
	    }

	    // called when the user selects a contextual menu item
	    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
	    	
	    	switch (item.getItemId()) {
	    	case R.id.menu_share:
	    		show();
	    		//Log.i(tag, "going to send email");
	    		
	    		// Sharing route in kml format.
		    	boolean showList = false;
			    List<Intent> targetedShareIntents = new ArrayList<Intent>();
		        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
		        shareIntent.setType("application/vnd.google-earth.kml+xml");
		        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(shareIntent, 0);
		        
		        for (ResolveInfo resolveInfo : resInfo) {
		        	String packageName = resolveInfo.activityInfo.packageName;
		        	 
		        	if (packageName.equalsIgnoreCase("com.facebook.katana")){
		        		 showList = true;
		        		 }
		        	 else if(packageName.equals("com.htc.android.mail")){
		        		 showList = true;
		        	 }
		        	 else if (packageName.equals("com.google.android.gm")){
		        		 showList = true;
		        	 }
		        	 else if (packageName.equals("com.twitter.android")){
		        		 showList = true;
		        	 }
		        	 else if (packageName.equals("com.linkedin.android")){
		        		 showList = true;
		        	 }
		        }
		        
		        String path = exportRoute(selectedId, selectedItemTitle);

		    	
		    	if (showList){	

					String fileName=parseTripName(selectedItemTitle);
					String directory=Environment.getExternalStorageDirectory() + "/MapMyTrip";
					List<String> files=getFilesWithExtension(directory,fileName,"mp4");
					if(path != null) {
						files.add(path);
					}
					sendMarkedFiles(files);

				}
		    	else 
		    	//if(!showList)
		    		dialogBuilder.setTitle(R.string.error_sending_message_title) //set the Title text
		 	    	.setMessage(R.string.error_sending_message)
		 	    	.setNeutralButton("OK", null).show();
			    
	    		
	    		mode.finish();	//close CAB
	    		return true;
	        
	    	case R.id.menu_delete:

	    		deleteListRecord(selectedItem, selectedItemTitle);
				deleteVideoRecordings(selectedItemTitle);
	    		mode.finish();
	    		
	    		return true;
	        
	    	default:
	    		return false;
	    	}
	    }

	    // called when the user exits the action mode
	    public void onDestroyActionMode(ActionMode mode) {
	    	mActionMode = null;
	    	selectedItem = -1;
	    }
	};



	private void deleteListRecord(int id, String title) {
		
		new AsyncTask<Object, Void, Cursor>() {
			
			int recordId = -1;
			int listItemId = 0;
			String recordTitle = null;
			
			@Override
			protected Cursor doInBackground(Object... params) {
				listItemId = (Integer) params[0];
				recordTitle = (String) params[1];

				String tempTripName = parseTripName(recordTitle);
				recordId = db.getRecordId(tempTripName);

				if(recordId != -1)
					db.deleteTrip(recordId);


				return null;
			}

			@Override
			protected void onPostExecute(Cursor cursor) {
				cursor = db.getRouteCount();
				historyAdapter = new HistoryAdapter(HistoryActivity.this, cursor);
				setListAdapter(historyAdapter);
			}

		}.execute(id, title);
	}//endAsyncTask


	public String parseString(String recordTitle) {
		StringTokenizer st = null;
		String tripName = null;
		int tokenCount = 0;
		
		try {
			st = new StringTokenizer(recordTitle, "_");
	    	
	    	while (st.hasMoreTokens()) {
	    		if(0 == tokenCount)
	    			tripName = st.nextToken() + "_";
	    		else if(1 == tokenCount)
	    			tripName = tripName + st.nextToken();
	    		
	    		tokenCount++;
	    	}
	    	
		}catch(Exception e) {
			MapMyTripApp.logToFile(getApplicationContext(), MapMyTripApp.LOG_TAG_ERROR, "Exception while string parse of temp trip name:"+e.getMessage());
			//i(tag, "Failed to parse string: " + recordTitle);
			return null;
		}
		
		if(tokenCount < 3)
			return null;
		
		return tripName;
	}

	public String parseTripName(String tripName){
		String tmpTripName=null;
		try{

			tmpTripName=tripName.substring(0,ordinalIndexOf(tripName,'_',1));

		}catch(Exception e){
			e.printStackTrace();
			MapMyTripApp.logToFile(getApplicationContext(), MapMyTripApp.LOG_TAG_ERROR, "Exception while string parse of temp trip name:"+e.getMessage());

			return null;
		}
	return tmpTripName;
	}

	public static int ordinalIndexOf(String str, char c, int n)throws Exception{
		int pos = str.indexOf(c, 0);
		while (n-- > 0 && pos != -1)
			pos = str.indexOf(c, pos+1);
		return pos;
	}


	private String exportRoute(int id, String tripName) {
			// export the db contents to a kml file
			Cursor cursor = null;
			String filePath = null;
			
			try {
				
				cursor = db.getRoute(id);
				
				int gmtTimestampColumnIndex = cursor.getColumnIndexOrThrow(MapTripDatabase.PointsColumn.TIMESTAMP);
				int latitudeColumnIndex = cursor.getColumnIndexOrThrow(MapTripDatabase.PointsColumn.LATITUDE);
	            int longitudeColumnIndex = cursor.getColumnIndexOrThrow(MapTripDatabase.PointsColumn.LONGITUDE);
	            int altitudeColumnIndex = cursor.getColumnIndexOrThrow(MapTripDatabase.PointsColumn.ALTITUDE);
	            int accuracyColumnIndex = cursor.getColumnIndexOrThrow(MapTripDatabase.PointsColumn.ACCURACY);
				
	            if (cursor != null) {
	            	cursor.moveToFirst();
	            	
					StringBuffer fileBuf = new StringBuffer();
					String beginTimestamp = null;
					String endTimestamp = null;
					String gmtTimestamp = null;
					initFileBuf(fileBuf, initValuesMap(tripName));
					
					do {
						gmtTimestamp = cursor.getString(gmtTimestampColumnIndex);
						
						if (beginTimestamp == null) {
							beginTimestamp = gmtTimestamp;
						}
						
						double latitude = cursor.getDouble(latitudeColumnIndex);
						double longitude = cursor.getDouble(longitudeColumnIndex);
						double altitude = cursor.getDouble(altitudeColumnIndex); //+ altitudeCorrectionMeters;
						double accuracy = cursor.getDouble(accuracyColumnIndex);
						fileBuf.append(sevenSigDigits.format(longitude)+","+sevenSigDigits.format(latitude)+","+altitude+"\n");
					} while (cursor.moveToNext());
					
					endTimestamp = gmtTimestamp;
					closeFileBuf(fileBuf, beginTimestamp, endTimestamp);
					String fileContents = fileBuf.toString();
					//d(tag, fileContents);
					
					
                    if (isSdReadable())   // isSdReadable()e method is define at bottom of the post
                    {
                    	String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath();
                    	 
                    	File sdDir = new File(fullPath + "/" + MapTripUtility.directoryName);
     					sdDir.mkdirs();
     					
     					String fileName = tripName.replace(":", "_");
     					fileName = fileName.replace("/", "_");
     					File file = new File(sdDir + File.separator  + fileName + MapTripUtility.fileExtension);
     					//File myFile = new File(sdDir + File.separator + "/"+ tripName);
     					
     					filePath = file.getAbsolutePath();
     					FileOutputStream fOut = new FileOutputStream(file);
                        OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                        myOutWriter.append(fileContents);
                        myOutWriter.close();
                        fOut.close();
                    }
                    
	    			Toast.makeText(getBaseContext(), "Export completed!", Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(getBaseContext(),
							"No location points found in the database, so no KML file was exported.",
							Toast.LENGTH_LONG).show();
					filePath = null;
				}
			} catch (FileNotFoundException fnfe) {
				Toast.makeText(getBaseContext(),
						"Error trying access the SD card.  Make sure your handset is not connected to a computer and the SD card is properly installed",
						Toast.LENGTH_LONG).show();
				filePath = null;
			} catch (Exception e) {
				Toast.makeText(getBaseContext(),
						"Error trying to export: " + e.getMessage(),
						Toast.LENGTH_LONG).show();
				filePath = null;
			} finally {
				if (cursor != null && !cursor.isClosed()) {
					cursor.close();
				}
			}
			
			return filePath;
		}
	   
	   public boolean isSdReadable() {
           
		   boolean mExternalStorageAvailable = false;
           
		   try {
			   String state = Environment.getExternalStorageState();

               if (Environment.MEDIA_MOUNTED.equals(state))
               {
                   // We can read and write the media
                   mExternalStorageAvailable = true;
                   //i(tag, "External storage card is readable.");
               }
               else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) 
               {
                   // We can only read the media
                   //i(tag, "External storage card is readable.");
                   mExternalStorageAvailable = true;
               } 
               else
               {
                   // Something else is wrong. It may be one of many other
                   // states, but all we need to know is we can neither read nor
                   // write
                   mExternalStorageAvailable = false;
                   Toast.makeText(getBaseContext(),
   						"Error trying access the SD card.  Make sure your handset is not connected to a computer and the SD card is properly installed",
   						Toast.LENGTH_LONG).show();
               }
           } catch (Exception e) {
        	   Toast.makeText(getBaseContext(),
						"Error trying access the SD card.  Make sure your handset is not connected to a computer and the SD card is properly installed",
						Toast.LENGTH_LONG).show();
           }
           return mExternalStorageAvailable;
       }
	   
	   private HashMap initValuesMap(String currentTripName) {
			HashMap valuesMap = new HashMap();

			valuesMap.put("FILENAME", currentTripName);
			
			//RadioButton airButton = (RadioButton)findViewById(R.id.RadioAir);
			if (false) { //airButton.isChecked()) {
				// use air settings
				valuesMap.put("EXTRUDE", "1");
				valuesMap.put("TESSELLATE", "0");
				valuesMap.put("ALTITUDEMODE", "absolute");
			} else {
				// use ground settings for the export
				valuesMap.put("EXTRUDE", "0");
				valuesMap.put("TESSELLATE", "1");
				valuesMap.put("ALTITUDEMODE", "absolute");
			}
			
			return valuesMap;
		}
	   
	   private void initFileBuf(StringBuffer fileBuf, HashMap valuesMap) {
			fileBuf.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			fileBuf.append("<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n");
			fileBuf.append("  <Document>\n");
			fileBuf.append("    <name>"+valuesMap.get("FILENAME")+"</name>\n");
			fileBuf.append("    <description>Map My Trip KML export</description>\n");
			fileBuf.append("    <Style id=\"redLineBluePoly\">\n");
			fileBuf.append("      <LineStyle>\n");
			fileBuf.append("        <color>ff0000ff</color>\n");
			fileBuf.append("        <width>10</width>\n");
			fileBuf.append("      </LineStyle>\n");
			fileBuf.append("      <PolyStyle>\n");
			fileBuf.append("        <color>7f00ff00</color>\n");
			fileBuf.append("      </PolyStyle>\n");
			fileBuf.append("    </Style>\n");
			fileBuf.append("    <Placemark>\n");
			fileBuf.append("      <name>Absolute Extruded</name>\n");
			fileBuf.append("      <description>Transparent green wall with yellow points</description>\n");
			fileBuf.append("      <styleUrl>#redLineBluePoly</styleUrl>\n");
			fileBuf.append("      <LineString>\n");
			fileBuf.append("        <extrude>"+valuesMap.get("EXTRUDE")+"</extrude>\n");
			fileBuf.append("        <tessellate>"+valuesMap.get("TESSELLATE")+"</tessellate>\n");
			fileBuf.append("        <altitudeMode>"+valuesMap.get("ALTITUDEMODE")+"</altitudeMode>\n");
			fileBuf.append("        <coordinates>\n");
		}
		
		private void closeFileBuf(StringBuffer fileBuf, String beginTimestamp, String endTimestamp) {
			fileBuf.append("        </coordinates>\n");
			fileBuf.append("     </LineString>\n");
			fileBuf.append("	 <TimeSpan>\n");
			String formattedBeginTimestamp = zuluFormat(beginTimestamp);
			fileBuf.append("		<begin>"+formattedBeginTimestamp+"</begin>\n");
			String formattedEndTimestamp = zuluFormat(endTimestamp);
			fileBuf.append("		<end>"+formattedEndTimestamp+"</end>\n");
			fileBuf.append("	 </TimeSpan>\n");
			fileBuf.append("    </Placemark>\n");
			fileBuf.append("  </Document>\n");
			fileBuf.append("</kml>");
		}
		
		private String zuluFormat(String beginTimestamp) {
			// turn 20081215135500 into 2008-12-15T13:55:00Z
			StringBuffer buf = new StringBuffer(beginTimestamp);
			buf.insert(4, '-');
			buf.insert(7, '-');
			buf.insert(10, 'T');
			buf.insert(13, ':');
			buf.insert(16, ':');
			buf.append('Z');
			return buf.toString();
		}
	
	private void show() {
		Toast.makeText(HistoryActivity.this,
				String.valueOf(selectedItem), Toast.LENGTH_LONG).show();
	}

	private void deleteVideoRecordings(String title){
		String fileName=parseTripName(title);
		String directory=Environment.getExternalStorageDirectory() + "/MapMyTrip";
		deleteFilesWithExtension(directory, fileName, "mp4");
	}

	public void deleteFilesWithExtension(final String directoryName,final String fileName, final String extension) {

		final File dir = new File(directoryName);
		final String[] allFiles = dir.list();
		for (final String file : allFiles) {
			if (file.startsWith(fileName) && file.endsWith(extension)) {
				new File(directoryName + "/" + file).delete();
			}
		}
	}

	protected boolean packageMarkedFiles(Intent aIntent,List<String>filePaths) {
		ArrayList<Uri> theUris = new ArrayList<Uri>();
		for (String file : filePaths)
		{
			File fileIn = new File(file);
			Uri u = Uri.fromFile(fileIn);
			theUris.add(u);
		}
		//iterate through the list and get overall mimeType
		String theOverallMIMEtype = null;
		String theMIMEtype = null;
		String theOverallMIMEcat=null;
		Iterator<Uri> iu = theUris.iterator();
		while (iu.hasNext()) {
			String theFilename = iu.next().getLastPathSegment();
			theMIMEtype = getMIMEtype(theFilename);
			if (theOverallMIMEtype!=null) {
				if (!theOverallMIMEtype.equals(theMIMEtype)) {
					theOverallMIMEcat = getMIMEcategory(theOverallMIMEtype);
					String theMIMEcat = getMIMEcategory(theMIMEtype);
					if (!theOverallMIMEcat.equals(theMIMEcat)) {
						theOverallMIMEtype = "multipart/mixed";
						break;  //no need to keep looking at the various types
					} else {
						theOverallMIMEtype = theOverallMIMEcat+"/*";
					}
				} else {
					//nothing to do
				}
			} else {
				theOverallMIMEtype = theMIMEtype;
			}
		}
		//theOverallMIMEtype = "multipart/mixed/*";
		if (theUris!=null && theOverallMIMEtype!=null) {
			aIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM,theUris);
			aIntent.setType(theOverallMIMEtype);
			return true;
		} else {
			return false;
		}
	}
	protected boolean sendMarkedFiles(List<String> list) {
		Intent theIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
		theIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] { "simplicitylabs@gmail.com" });
		theIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"Video Recordings (mp4) and Map Route (kml) files");
		theIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Map Route (kml) and video recordings (mp4) files are attached herewith.");
		if (packageMarkedFiles(theIntent,list)) {
			startActivity(Intent.createChooser(theIntent, "Send Email"));
		}
		return true;
	}
	private String getMIMEtype(String path) {

		String extension = path.substring(path.lastIndexOf("."));
		String mimeTypeMap = MimeTypeMap.getFileExtensionFromUrl(extension);
		String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(mimeTypeMap);
		return mimeType;
	}
	public static String getMIMEcategory(String aMIMEtype) {
		if (aMIMEtype!=null) {
			aMIMEtype = aMIMEtype.substring(0,aMIMEtype.lastIndexOf("/",aMIMEtype.length()-1))+"/*";
		} else {
			aMIMEtype = "*/*";
		}
		return aMIMEtype;
	}

	public List<String> getFilesWithExtension(final String directoryName,final String fileName, final String extension) {
		List<String>list=new ArrayList<String>();
		final File dir = new File(directoryName);
		final String[] allFiles = dir.list();
		for (final String file : allFiles) {
			if (file.startsWith(fileName) && file.endsWith(extension)) {
				list.add(directoryName + "/" + file);
			}
		}
		return list;
	}

}
