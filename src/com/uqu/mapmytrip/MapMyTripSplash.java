package com.uqu.mapmytrip;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.simplicity.io.mapmytrip.R;
import com.uqu.login.LoginActivity;


public class MapMyTripSplash extends Activity {
		
	protected int _splashTime = 5000; 
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    Configuration configuration = getResources().getConfiguration();
            
	    setContentView(R.layout.splash_activity);
	    ImageView splashImage= (ImageView) findViewById(R.id.splashImage);
	    if ((configuration.screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
        	//setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
        	splashImage.setImageDrawable(getResources().getDrawable(R.drawable.splash));
	    }
	   
	    
	    splashImage.setScaleType(ScaleType.FIT_XY);
	    
	    Handler handler = new Handler();
	    
        // run a thread after 2 seconds to start the home screen
        handler.postDelayed(new Runnable() {
 
            @Override
            public void run() {
 
                // make sure we close the splash screen so the user won't come back when it presses back key
 
                finish();
                // start the home screen
 
                Intent intent = new Intent(MapMyTripSplash.this, LoginActivity.class);
                MapMyTripSplash.this.startActivity(intent);
            }
 
        }, 3000);
	}
	
}
