/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.uqu.wizard.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Represents a single line item on the final review page.
 *
 * @see com.simplicity.madina.survey.wizard.ui.ReviewFragment
 */
public class ReviewItem implements Parcelable {
    public static final int DEFAULT_WEIGHT = 0;

    private int mWeight;
    private String mTitle;
    private String mDisplayValue;
    private String mPageKey;

    public ReviewItem(String title, String displayValue, String pageKey) {
        this(title, displayValue, pageKey, DEFAULT_WEIGHT);
    }

    public ReviewItem(String title, String displayValue, String pageKey, int weight) {
        mTitle = title;
        mDisplayValue = displayValue;
        mPageKey = pageKey;
        mWeight = weight;
    }
    public ReviewItem(Parcel in){

        this.mWeight = in.readInt();
        this.mTitle = in.readString();
        this.mDisplayValue = in.readString();
        this.mPageKey = in.readString();
    }


    public String getDisplayValue() {
        return mDisplayValue;
    }

    public void setDisplayValue(String displayValue) {
        mDisplayValue = displayValue;
    }

    public String getPageKey() {
        return mPageKey;
    }

    public void setPageKey(String pageKey) {
        mPageKey = pageKey;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public int getWeight() {
        return mWeight;
    }

    public void setWeight(int weight) {
        mWeight = weight;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mWeight);
        dest.writeString(mTitle);
        dest.writeString(mDisplayValue);
        dest.writeString(mPageKey);

    }
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ReviewItem createFromParcel(Parcel in) {
            return new ReviewItem(in);
        }

        public ReviewItem[] newArray(int size) {
            return new ReviewItem[size];
        }
    };
}


